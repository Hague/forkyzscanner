
package app.crossword.yourealwaysbe.forkyzscanner.view

import android.content.Context
import android.util.AttributeSet
import androidx.core.text.HtmlCompat
import com.google.android.material.materialswitch.MaterialSwitch

import app.crossword.yourealwaysbe.forkyzscanner.R

class SettingSwitchView(context : Context, attrs : AttributeSet)
        : MaterialSwitch(context, attrs) {

    init {
        val ta = context.obtainStyledAttributes(
            attrs, R.styleable.SettingSwitchView, 0, 0
        )
        try {
            val title = ta.getString(R.styleable.SettingSwitchView_title) ?: ""
            val summary = ta.getString(R.styleable.SettingSwitchView_summary) ?: ""

            // TODO: make a proper layout one day, this is a bit of a
            // bodge
            val formatted = context.getString(R.string.switch_format, title, summary)
            setText(HtmlCompat.fromHtml(formatted, HtmlCompat.FROM_HTML_MODE_COMPACT))
        } finally {
            ta.recycle()
        }
    }
}
