
package app.crossword.yourealwaysbe.forkyzscanner.androidversion

import android.content.Context
import android.graphics.Bitmap
import android.net.Uri
import android.os.Build

import java.io.IOException

val versionUtils : AndroidVersionUtils by lazy {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
        TiramisuUtils()
    } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
        PieUtils()
    } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
        MarshmallowUtils()
    } else {
        LollipopUtils()
    }
}

abstract class AndroidVersionUtils {
    @Throws(IOException::class)
    abstract fun getBitmapFromUri(context : Context, uri : Uri) : Bitmap
    abstract fun hasNetworkConnection(context : Context) : Boolean
    abstract fun getApplicationVersionName(context : Context) : String
}
