
package app.crossword.yourealwaysbe.forkyzscanner

import android.net.Uri
import android.os.Bundle
import android.view.ActionMode
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

import app.crossword.yourealwaysbe.forkyzscanner.databinding.ClueListItemBinding
import app.crossword.yourealwaysbe.forkyzscanner.databinding.CluesFragmentBinding
import app.crossword.yourealwaysbe.forkyzscanner.model.Puzzle.Clue
import app.crossword.yourealwaysbe.forkyzscanner.model.Puzzle.ClueAdded
import app.crossword.yourealwaysbe.forkyzscanner.model.Puzzle.ClueChanged
import app.crossword.yourealwaysbe.forkyzscanner.model.Puzzle.ClueDeleted
import app.crossword.yourealwaysbe.forkyzscanner.model.Puzzle.ClueList
import app.crossword.yourealwaysbe.forkyzscanner.model.Puzzle.ClueListEvent
import app.crossword.yourealwaysbe.forkyzscanner.model.Puzzle.ClueListListener
import app.crossword.yourealwaysbe.forkyzscanner.model.Puzzle.ClueMoved

public abstract class CluesFragment(
    private val list : ClueList,
    private val clueListLabelID : Int,
) : ScannerFragment() {

    class Across() : CluesFragment(ClueList.ACROSS, R.string.across_clues)
    class Down() : CluesFragment(ClueList.DOWN, R.string.down_clues)

    private lateinit var viewModel : ForkyzScannerViewModel
    private lateinit var binding : CluesFragmentBinding
    private lateinit var adapter : ClueListAdapter
    private lateinit var listener : ClueListListener

    private var actionMode : ActionMode? = null
    private val actionModeCallback = object : ActionMode.Callback {
        override fun onCreateActionMode(mode : ActionMode, menu : Menu)
                : Boolean {
            actionMode = mode
            activity?.getMenuInflater()
                ?.inflate(R.menu.clues_action_menu, menu)
            return true
        }

        override fun onPrepareActionMode(
            actionMode : ActionMode,
            menu : Menu,
        ) : Boolean = false

        override fun onActionItemClicked(
            actionMode : ActionMode,
            menuItem : MenuItem,
        ) : Boolean {
            when (menuItem.getItemId()) {
                R.id.clues_action_delete -> {
                    viewModel.deleteMultiSelectedClues(list)
                    actionMode.finish()
                }
                R.id.clues_action_move_up ->
                    viewModel.moveMultiSelectedCluesUp(list)
                R.id.clues_action_move_down ->
                    viewModel.moveMultiSelectedCluesDown(list)
            }
            return true
        }

        override fun onDestroyActionMode(mode : ActionMode) {
            viewModel.clearMultiSelection(list)
            actionMode = null
        }
    }

    override fun onCreateView(
        inflater : LayoutInflater,
        container : ViewGroup?,
        savedInstanceState : Bundle?,
    ) : View {
        val provider = ViewModelProvider(requireActivity())
        viewModel = provider.get(ForkyzScannerViewModel::class.java)

        binding = CluesFragmentBinding.inflate(inflater, container, false)
        binding.lifecycleOwner = viewLifecycleOwner

        setupView()
        setupObservers()

        return binding.root
    }

    override fun onResume() {
        super.onResume()
        // more efficient than observing every box in the grid for changes
        // (since that would trigger setWarningText for every box in the grid
        // because observe always does an initial callback when a value has
        // been set)
        setWarningText()
    }

    protected override fun onImageGotten(imageUri : Uri) {
        viewModel.scanCluesFromUri(list, imageUri)
    }

    private fun setupView() {
        binding.clueList.layoutManager = LinearLayoutManager(context)
        binding.clueList.itemAnimator = DefaultItemAnimator()
        binding.clueList.addItemDecoration(
            DividerItemDecoration(context, DividerItemDecoration.VERTICAL)
        )

        adapter = ClueListAdapter(listOf<Clue>())
        binding.clueList.adapter = adapter

        binding.addClueButton.setOnClickListener {
            viewModel.puzzle.value?.addClue(list, Clue())
        }
        binding.scanCameraButton.setOnClickListener {
            viewModel.warnLanguageModelSetup { getPhoto() }
        }
        binding.scanImageButton.setOnClickListener {
            viewModel.warnLanguageModelSetup { getImage() }
        }

        binding.splitClueButton.setOnClickListener {
            viewModel.selectedClueIndex[list]?.value?.let { index ->
                val position = binding.hintText.selectionStart
                viewModel.puzzle.value?.splitClue(list, index, position)
            }
        }

        val clueListLabel = requireActivity().getString(clueListLabelID)
        binding.clueListLabel.setText(clueListLabel)
    }

    private fun setupObservers() {
        viewModel.puzzle.observe(viewLifecycleOwner, { onNewPuzzle() })
        viewModel.selectedClueIndex[list]?.observe(
            viewLifecycleOwner,
            { onNewSelectedClue() },
        )
        viewModel.multiSelectedClueIndices[list]?.observe(
            viewLifecycleOwner,
            { onNewMultiSelectedClues() },
        )
    }

    private fun onNewSelectedClue() {
        val clue = viewModel.getSelectedClue(list)
        if (clue == null) {
            binding.clueEditCard.visibility = View.GONE
        } else {
            binding.selectedClue = clue
            binding.clueEditCard.visibility = View.VISIBLE
        }
    }

    private fun onNewMultiSelectedClues() {
        if (viewModel.isMultiSelect(list)) {
            if (actionMode == null) {
                activity?.startActionMode(actionModeCallback)
            }
        } else {
            actionMode?.finish()
        }
    }

    private fun onNewPuzzle() {
        val puzzle = viewModel.puzzle.value

        if (puzzle == null) return

        val clues = puzzle.getClueList(list)

        adapter = ClueListAdapter(clues)
        binding.clueList.adapter = adapter

        onNewSelectedClue()
        setWarningText()

        listener = object : ClueListListener {
            override fun onClueListChange(
                list : ClueList,
                newList : List<Clue>,
                changed : List<ClueListEvent>
            ) {
                // because changes may come from a background thread
                activity?.runOnUiThread {
                    adapter.clues = newList
                    changed.forEach { change ->
                        when (change) {
                            is ClueChanged -> {
                                adapter.notifyItemChanged(change.index)
                            }
                            is ClueAdded -> {
                                adapter.notifyItemInserted(change.index)
                            }
                            is ClueMoved -> {
                                adapter.notifyItemMoved(change.src, change.dst)
                            }
                            is ClueDeleted -> {
                                adapter.notifyItemRemoved(change.index)
                            }
                        }
                    }
                    setWarningText()
                }
            }
        }

        puzzle.addClueListListener(list, listener)
    }

    private fun setWarningText() {
        viewModel.puzzle.value?.let { puzzle ->
            // work out which numbers are missing
            val numbers = puzzle.getNumbers(list)
            numbers.removeAll(
                puzzle.getClueList(list)
                    .map({ it.number.value?.toIntOrNull() })
                    .filterNotNull()
            )

            if (numbers.isEmpty()) {
                binding.warning = null
            } else {
                val numberList = numbers
                    .sorted()
                    .joinToString(
                        getString(R.string.warning_missing_numbers_sep)
                    )
                binding.warning = getString(
                    R.string.warning_missing_numbers,
                    numberList
                )
            }
        }
    }

    private inner class ClueListAdapter(var clues : List<Clue>)
            : RecyclerView.Adapter<ClueViewHolder>() {
        override fun onCreateViewHolder(
            parent : ViewGroup,
            viewType : Int,
        ) : ClueViewHolder {
            val binding =
                ClueListItemBinding.inflate(getLayoutInflater(), parent, false)
            binding.lifecycleOwner = (this@CluesFragment).viewLifecycleOwner
            return ClueViewHolder(binding)
        }

        override fun onBindViewHolder(
            holder : ClueViewHolder,
            position : Int
        ) {
            holder.clue = clues[position]
        }

        override fun onViewRecycled(holder : ClueViewHolder) {
            holder.clue = null
        }

        override fun getItemCount() : Int {
            return clues.size
        }
    }

    /**
     * Holds a clue!
     *
     * Assumes binding already has life cycle owner set
     */
    private inner class ClueViewHolder(val binding : ClueListItemBinding)
            : RecyclerView.ViewHolder(binding.root) {

        val numberObserver = { _ : String ->
            setWarningText()
            (this@CluesFragment).setWarningText()
        }

        var clue : Clue? = null
            set(value) {
                binding.clue?.number?.removeObserver(numberObserver)

                binding.clue = value

                binding.clue?.number?.observe(this@CluesFragment, numberObserver)

                binding.root.setOnClickListener {
                    val index = getBindingAdapterPosition()
                    if (viewModel.isMultiSelect(list)) {
                        viewModel.toggleMultiSelected(list, index)
                    } else {
                        viewModel.selectedClueIndex[list]?.let {
                            it.value = index
                        }
                    }
                }
                binding.root.setOnLongClickListener {
                    val index = getBindingAdapterPosition()
                    viewModel.toggleMultiSelected(list, index)
                    true
                }

                setChecked()
                setMultiSelected()
            }

        init {
            // not sure how to move this into a data binding
            // but would be nice if it was
            viewModel.selectedClueIndex[list]?.observe(
                (this@CluesFragment).viewLifecycleOwner,
                { setChecked() },
            )
            viewModel.multiSelectedClueIndices[list]?.observe(
                (this@CluesFragment).viewLifecycleOwner,
                { setMultiSelected() },
            )
            viewModel.puzzle.observe(
                (this@CluesFragment).viewLifecycleOwner,
                { setWarningText() }
            )
        }

        private fun setChecked() {
            val index = getBindingAdapterPosition()
            val selectedIndex = viewModel.selectedClueIndex[list]?.value ?: -1
            binding.clueHintView.setChecked(
                index >= 0 && index == selectedIndex
            )
        }

        private fun setMultiSelected() {
            viewModel.multiSelectedClueIndices[list]?.value?.let { indices ->
                val index = getBindingAdapterPosition()
                binding.clueListItemView.setSelected(index in indices)
            }
        }

        private fun setWarningText() {
            if (binding.clue == null)
                return

            val number = binding.clue?.number?.value?.toIntOrNull()

            if (number == null) {
                binding.warning = requireActivity().getString(
                    R.string.warning_number_not_numeric
                )
            } else if (
                !(viewModel.puzzle.value?.hasNumber(number, list) ?: false)
            ) {
                binding.warning = requireActivity().getString(
                    R.string.warning_number_not_in_grid
                )
            } else {
                binding.warning = null
            }
        }
    }
}
