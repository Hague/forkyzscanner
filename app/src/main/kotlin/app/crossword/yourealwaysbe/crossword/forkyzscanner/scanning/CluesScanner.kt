
package app.crossword.yourealwaysbe.forkyzscanner.scanning

import android.content.Context
import android.net.Uri

import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.io.InputStream

import com.googlecode.tesseract.android.TessBaseAPI

import app.crossword.yourealwaysbe.forkyzscanner.model.Puzzle.Clue

class CluesScanner {
    companion object {
        private val FILE_EXT = ".traineddata"

        /**
         * Language models installed in data dir that can be used
         *
         * Call off UI thread
         */
        fun getAvailableLanguageModels(context : Context) : List<String> {
            return (getTessDir(context).list() ?: arrayOf())
                .filter { it.endsWith(FILE_EXT) }
                .map { it.substring(0, it.length - FILE_EXT.length) }
        }

        fun hasLanguageModel(
            context : Context,
            languageName : String
        ) : Boolean = getLanguageModelFile(context, languageName).exists()

        /**
         * Imports model from input stream, saves with given filename
         *
         * Overwrites any existing with same name. Call off UI thread.
         * File name should end with .traineddata, else ignored.
         */
        @Throws(IOException::class)
        fun importLanguageModel(
            context : Context,
            filename : String,
            input : InputStream,
        ) {
            if (filename.endsWith(FILE_EXT)) {
                val tessDir = getTessDir(context)
                val dataFile = File(tessDir, filename)
                FileOutputStream(dataFile).use { input.copyTo(it) }
            }
        }

        /**
         * Delete model from data directory
         *
         * Probably should run on a background thread
         */
        @Throws(IOException::class)
        fun deleteLanguageModel(context : Context, languageName: String) {
            getLanguageModelFile(context, languageName).delete()
        }

        private fun getTessDataPath(context : Context) : String {
            return context.getFilesDir().getAbsolutePath()
        }

        /**
         * Get the directory of Tesseract data files
         *
         * Creates if doesn't exist
         */
        private fun getTessDir(context : Context) : File {
            val tessDir = File(getTessDataPath(context), "tessdata")
            if (!tessDir.exists()) {
                tessDir.mkdir()
            }
            return tessDir
        }

        private fun getLanguageModelFile(
            context : Context,
            languageName : String,
        ) : File = File(getTessDir(context), languageName + FILE_EXT)
    }

    private val STARTS_NUM_RE = "(\\d+)[.,:;\\-\\s](.*)".toRegex()
    private val STARTS_NUM_RE_NUM_GRP = 1
    private val STARTS_NUM_RE_HINT_GRP = 2

    private val ENDS_ENUM_RE = ".*\\([\\d,\\-'\\s]+\\)\\s*".toRegex()

    /**
     * Scan notes from bitmap
     *
     * Tries to remove unwanted line breaks in paragraphs
     *
     * Don't call on main thread!
     */
    fun scanNotesFromUri(
        context : Context,
        languageModelName : String,
        imageUri : Uri
    ) : String {
        val text = scanTextFromUri(context, languageModelName, imageUri)
        // remove line breaks that separate two non-space characters
        // if line ends with - don't insert a space, else do
        // and anything that looks like an indented para break gets an
        // extra line
        return text.replace("""[-‒–—]\n(?=[^\s])""".toRegex(), "")
            .replace("""(?<=[^\s])\n(?=[^\s])""".toRegex(), " ")
            .replace("""(?<=[^\s])\n\s+""".toRegex(), "\n\n")
    }

    /**
     * Scan clues from bitmap
     *
     * Must be a single column of text.
     *
     * Don't call on main thread!
     */
    fun scanCluesFromUri(
        context : Context,
        languageModelName : String,
        imageUri : Uri
    ) : List<Clue> {
        return getCluesFromText(
            scanTextFromUri(context, languageModelName, imageUri)
        )
    }

    /**
     * Scan raw text
     *
     * This is used for scanning notes, so doesn't really belong here. But it's
     * easier than making separate classes that share code...
     *
     * Don't call on main thread
     */
    private fun scanTextFromUri(
        context : Context,
        languageModelName : String,
        imageUri : Uri
    ) : String {
        val path = imageUri.getPath()
        if (path == null)
            return ""

        val imageFile = File(path)

        if (!imageFile.exists()) return ""
        if (!hasLanguageModel(context, languageModelName)) return ""

        val tess = TessBaseAPI()

        val inited = tess.init(
            getTessDataPath(context),
            languageModelName,
            TessBaseAPI.OEM_LSTM_ONLY,
        )

        if (!inited) {
            tess.recycle()
            return ""
        }

        tess.setImage(imageFile)
        val text = tess.getUTF8Text()

        tess.recycle()

        return text
    }

    private fun getCluesFromText(text : String) : List<Clue> {
        val clueList = mutableListOf<Clue>()

        var number = ""
        var hint = ""

        // Strategy: split by trimmed lines
        // If a line starts with something like 1. consider the previous clue
        // finished and add it to list.
        // If a line ends with something like (4) (an enumerate), consider it
        // the end of the current clue.
        // Otherwise, collect hint line-by-line.
        // Don't trust empty lines as clue separators since Tesseract is not
        // reliable enough to judge spacing.
        text.split("\\R".toRegex()).forEach { fullLine ->
            val line = fullLine.trim()

            if (!line.isEmpty()) {
                val m = STARTS_NUM_RE.matchEntire(line)
                if (m != null) {
                    addClueToList(clueList, number, hint)
                    number = m.groups
                        .get(STARTS_NUM_RE_NUM_GRP)
                        ?.value
                        ?.trim()
                        ?: ""
                    hint = m.groups
                        .get(STARTS_NUM_RE_HINT_GRP)
                        ?.value
                        ?.trim()
                        ?: ""
                } else {
                    hint += (if (hint.isEmpty()) "" else " ") + line
                }

                if (ENDS_ENUM_RE.matchEntire(hint) != null) {
                    addClueToList(clueList, number, hint)
                    number = ""
                    hint = ""
                }
            }
        }

        // last one if have one
        addClueToList(clueList, number, hint)

        return clueList
    }

    /**
     * Add the clue if not empty
     *
     * @param number must be a string of digits if not empty
     */
    private fun addClueToList(
        clueList : MutableList<Clue>,
        number : String,
        hint : String,
    ) {
        if (!number.isEmpty() || !hint.isEmpty()) {
            clueList.add(Clue(number, hint))
        }
    }
}
