
package app.crossword.yourealwaysbe.forkyzscanner

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.constraintlayout.widget.ConstraintSet
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider

import app.crossword.yourealwaysbe.forkyzscanner.databinding.GridNumbersFragmentBinding
import app.crossword.yourealwaysbe.forkyzscanner.model.Puzzle.Grid

public class GridNumbersFragment() : Fragment() {
    private val INIT_NEXT_NUM = "1"

    private lateinit var viewModel : ForkyzScannerViewModel
    private lateinit var binding : GridNumbersFragmentBinding

    override fun onCreateView(
        inflater : LayoutInflater,
        container : ViewGroup?,
        savedInstanceState : Bundle?,
    ) : View {
        val provider = ViewModelProvider(requireActivity())
        viewModel = provider.get(ForkyzScannerViewModel::class.java)

        binding = GridNumbersFragmentBinding.inflate(inflater, container, false)
        binding.viewModel = viewModel
        binding.lifecycleOwner = viewLifecycleOwner

        setupView()
        setupObservers()

        return binding.root
    }

    private fun setupView() {
        binding.applyStandardNumberingButton
            .setOnClickListener {
                viewModel.puzzle
                    .value?.grid?.value?.overwriteNumbersStandard()
            }
    }

    private fun setupObservers() {
        viewModel.puzzle.observe(
            viewLifecycleOwner,
            { puzzle ->
                puzzle.grid.observe(
                    viewLifecycleOwner,
                    { grid -> onNewGrid(grid) }
                )
            }
        )
    }

    private fun onNewGrid(grid : Grid) {
        val set = ConstraintSet()
        set.clone(binding.constraintLayout)
        set.setDimensionRatio(
            R.id.grid_buttons,
            "" + grid.width + ":" + grid.height
        )
        set.applyTo(binding.constraintLayout)

        binding.gridButtons.setGrid(
            grid,
            { box ->
                if (box.number.value == null) {
                    viewModel.nextCellNumber.value?.let { number ->
                        box.number.value = number
                        viewModel.nextCellNumber.value = number + 1
                    }
                } else {
                    box.number.value = null
                }
            },
        )
    }
}
