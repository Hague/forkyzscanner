
package app.crossword.yourealwaysbe.forkyzscanner

import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.databinding.BindingAdapter
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.google.android.material.datepicker.MaterialDatePicker

import java.time.Instant
import java.time.LocalDate
import java.time.ZoneId
import java.time.format.DateTimeFormatter

import app.crossword.yourealwaysbe.forkyzscanner.databinding.MetadataFragmentBinding

private val dateFormatter = DateTimeFormatter.ofPattern("dd MMM yyyy")

@BindingAdapter("puzzledate")
fun setPuzzleDate(view: View, date : LocalDate?) {
    val tv = view as TextView
    tv.text = if (date == null) "" else dateFormatter.format(date)
}

public class MetadataFragment() : ScannerFragment() {
    private lateinit var binding : MetadataFragmentBinding
    private lateinit var viewModel : ForkyzScannerViewModel

    override fun onCreateView(
        inflater : LayoutInflater,
        container : ViewGroup?,
        savedInstanceState : Bundle?,
    ) : View {
        val provider = ViewModelProvider(requireActivity())
        viewModel = provider.get(ForkyzScannerViewModel::class.java)

        binding = MetadataFragmentBinding.inflate(inflater, container, false)
        binding.lifecycleOwner = viewLifecycleOwner
        binding.viewModel = viewModel

        setupViews()

        return binding.root
    }

    protected override fun onImageGotten(imageUri : Uri) {
        viewModel.scanNotesFromUri(imageUri)
    }

    private fun setupViews() {
        binding.puzzleDateButton.setOnClickListener { pickDate() }
        binding.puzzleDate.setOnClickListener { pickDate() }
        binding.newPuzzleButton.setOnClickListener { viewModel.newPuzzle() }
        binding.scanNotesCameraButton.setOnClickListener {
            viewModel.warnLanguageModelSetup { getPhoto() }
        }
        binding.scanNotesImageButton.setOnClickListener {
            viewModel.warnLanguageModelSetup { getImage() }
        }
    }

    private fun pickDate() {
        val picker = MaterialDatePicker
            .Builder
            .datePicker()
            .setTitleText(R.string.pick_date_title)
            .build()

        picker.addOnPositiveButtonClickListener { date ->
            val localDate = Instant.ofEpochMilli(date)
                .atZone(ZoneId.systemDefault())
                .toLocalDate()
            viewModel.puzzle.value?.let { it.date.value = localDate }
        }

        picker.show(requireActivity().supportFragmentManager, "DatePicker")
    }
}
