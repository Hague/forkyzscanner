
package app.crossword.yourealwaysbe.forkyzscanner

import android.app.Activity
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.pdf.PdfRenderer
import android.net.Uri
import android.os.ParcelFileDescriptor
import androidx.activity.result.contract.ActivityResultContracts.OpenDocument
import androidx.activity.result.contract.ActivityResultContracts.StartActivityForResult
import androidx.activity.result.contract.ActivityResultContracts.TakePicture
import androidx.core.content.FileProvider
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import kotlinx.coroutines.launch

import java.io.File
import java.io.FileOutputStream

import com.yalantis.ucrop.UCrop
import com.yalantis.ucrop.UCrop.Companion.getOutput
import com.yalantis.ucrop.UCrop.Companion.of

public abstract class ScannerFragment() : Fragment() {
    private val CACHE_PDF_FILENAME = "cached.pdf"
    private val CAMERA_FILENAME = "camera.jpg"
    private val CROPPED_FILENAME = "cropped.jpg"

    private val MIME_PDF = "application/pdf"
    private val MIME_IMAGE = "image/*"

    private val cameraUri by lazy {
        val context = requireActivity()
        FileProvider.getUriForFile(
            context,
            context.getApplicationContext().getPackageName() + ".provider",
            File(requireActivity().getCacheDir(), CAMERA_FILENAME),
        )
    }
    private val croppedUri by lazy {
        Uri.fromFile(
            File(requireActivity().getCacheDir(), CROPPED_FILENAME)
        )
    }

    private val getImageLauncher =
        registerForActivityResult(OpenDocument()) { uri : Uri? ->
            uri?.let {
                val mime = requireActivity().getContentResolver().getType(uri)
                if (mime == MIME_PDF) {
                    convertPdfAndCrop(uri)
                } else {
                    cropUri(it)
                }
            }
        }

    private val getPhotoLauncher =
        registerForActivityResult(TakePicture()) { success : Boolean ->
            if (success) cropUri(cameraUri)
        }

    private val ucropLauncher =
        registerForActivityResult(StartActivityForResult()) { result ->
            if (result.resultCode == Activity.RESULT_OK) {
                val imageUri : Uri = UCrop.getOutput(result.data!!)!!
                onImageGotten(imageUri)
            }
        }

    protected fun getPhoto() {
        getPhotoLauncher.launch(cameraUri)
    }

    protected fun getImage() {
        getImageLauncher.launch(arrayOf(MIME_IMAGE, MIME_PDF))
    }

    protected abstract fun onImageGotten(imageUri : Uri)

    private fun cropUri(uri : Uri) {
        val options = UCrop.Options()
        options.setFreeStyleCropEnabled(true)
        options.withAspectRatio(1F, 1F)
        UCrop.of(uri, croppedUri)
            .withOptions(options)
            .start(requireActivity(), ucropLauncher)
    }

    private fun convertPdfAndCrop(uri : Uri) {
        lifecycleScope.launch {
            try {
                // bit of a monster:
                // 1. copy pdf to cache file
                // 2. open cache file with a seekable file descriptor
                // 3. pdf render first page of file to a bitmap
                // 4. write bitmap to the camera file uri
                // 5. crop the camera file uri
                val activity = requireActivity()
                val cached = File(
                    activity.getCacheDir(),
                    CACHE_PDF_FILENAME,
                )
                val cr = requireActivity().getContentResolver()
                cr.openInputStream(uri).use { input ->
                    FileOutputStream(cached).use { output ->
                        input?.copyTo(output)
                    }
                }
                ParcelFileDescriptor.open(
                    cached,
                    ParcelFileDescriptor.MODE_READ_ONLY,
                ).use { pfd ->
                    val renderer = PdfRenderer(pfd)

                    if (renderer.getPageCount() > 0) {
                        val page = renderer.openPage(0)
                        val bitmap = Bitmap.createBitmap(
                            page.width * 200 / 72,
                            page.height * 200 / 72,
                            Bitmap.Config.ARGB_8888,
                        )
                        bitmap.eraseColor(Color.WHITE)
                        page.render(
                            bitmap,
                            null,
                            null,
                            PdfRenderer.Page.RENDER_MODE_FOR_DISPLAY,
                        )

                        page.close()
                        renderer.close()

                        val outFile = File(
                            activity.getCacheDir(),
                            CAMERA_FILENAME,
                        )
                        FileOutputStream(outFile).use { output ->
                            bitmap.compress(
                                Bitmap.CompressFormat.PNG,
                                80,
                                output,
                            )
                        }

                        cropUri(cameraUri)
                    }
                }
            } catch (e : Exception) {
                // we tried
            }
        }
    }
}
