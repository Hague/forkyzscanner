
# Forkyz Scanner

An Android app for scanning crossword puzzles from the camera, image files, or
PDF. The puzzle can then be shared as an IPuz file.

Forkyz Scanner is a companion app to the [Forkyz][forkyz] crossword app so
that you can import any puzzle into Forkyz (or the crossword app of your
choice).

Scanning is a multi-step process: scan the grid, then the columns of clues. The
scan results can be edited to make corrections.

The app is somewhat heavyweight: it includes OpenCV for image processing and
Tesseract for text recognition. Language packs for text recognition must be
downloaded from settings before this feature can be used.

## Screenshots

<img
    alt="The main screen for scanning puzzles. The grid is in the centre, with size adjustment buttons around the outside. Buttons at the bottom allow scanning from the camera or files."
    src="fastlane/metadata/android/en-US/images/phoneScreenshots/01-grid-scanning.png"
    width=150
/>
<img
    alt="The main screen for scanning clues. The clue list has edit boxes above it for changing clues. At the bottom there are buttons for adding and scanning clues."
    src="fastlane/metadata/android/en-US/images/phoneScreenshots/02-clue-scanning.png"
    width=150
/>
<img
    alt="The metadata screen. It has title, author, date, and notes fields. There is also a button for resetting the puzzle."
    src="fastlane/metadata/android/en-US/images/phoneScreenshots/03-metadata.png"
    width=150
/>
<img
    alt="Cropping images to get the grid. The screen shows a crossword file image with the grid selected."
    src="fastlane/metadata/android/en-US/images/phoneScreenshots/04-grid-cropping.png"
    width=150
/>
<img
    alt="The number editing screen. The grid is shown with numbers, there is a next number field at the top and a button at the bottom for applying standard crossword numbering."
    src="fastlane/metadata/android/en-US/images/phoneScreenshots/05-number-editing.png"
    width=150
/>

## Changes

[Changelogs are available here][changelogs].

## Building and Testing

Build as normal with gradle:

    ./gradlew assembleRelease

Or install the debug version with phone plugged in:

    ./gradlew installDebug

Tests are connected Android tests, so also with a phone plugged in:

    ./gradlew connectedAndroidTest

They need to be connected so e.g. the OpenCV library works. (It fails without,
i tried.)

## Alternatives

Forkyz Scanner is quite heavyweight: aiming to use image processing and text
recognition to enable fast scanning of puzzles. This needs memory and
processing power and the results can be inexact.

From FDroid, you might also try

* [Puzzle Grid][puzzle-grid] -- for entering grids (with automatic support for
  symmetry), photographing clues, and completing the puzzle. Also supports
  other puzzle types.

## License

Licensed under the MIT license.

ipuz is a trademark of Puzzazz, used with permission

[changelogs]: https://gitlab.com/Hague/forkyzscanner/-/blob/main/CHANGELOG.md
[forkyz]: https://gitlab.com/Hague/forkyz
[puzzle-grid]: https://bitbucket.org/Fyll-nds/puzzle-grid/src/master/
