
package app.crossword.yourealwaysbe.forkyzscanner.util

import android.content.Context
import android.net.Uri
import android.provider.OpenableColumns

import java.io.BufferedInputStream
import java.io.InputStream

class FileHandling {
    companion object {
        /**
         * Opens an input stream from content resolver
         */
        fun openInputStream(context : Context, uri : Uri) : InputStream {
            return BufferedInputStream(
                context.getContentResolver().openInputStream(uri)
            )
        }

        fun deleteFile(context : Context, uri : Uri) {
            context.getContentResolver().delete(uri, null, null)
        }

        /**
         * Queries for file name of uri
         */
        fun getFilename(context : Context, uri : Uri) : String? {
            val returnCursor = context.getContentResolver()
                     .query(uri, null, null, null, null)

            if (returnCursor == null)
                return null

            val nameIndex =
                returnCursor.getColumnIndex(OpenableColumns.DISPLAY_NAME)

            returnCursor.moveToFirst()
            val name = returnCursor.getString(nameIndex)
            returnCursor.close()

            return name
        }
    }
}
