
package app.crossword.yourealwaysbe.forkyzscanner

import android.app.DownloadManager
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import androidx.core.content.ContextCompat
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

import java.io.IOException

import app.crossword.yourealwaysbe.forkyzscanner.scanning.CluesScanner
import app.crossword.yourealwaysbe.forkyzscanner.util.FileHandling

class LanguageModelDownloadReceiver : BroadcastReceiver() {
    companion object {
        /**
         * Broadcast intent to say it's time to refresh model list
         */
        val LANGUAGE_MODEL_DOWNLOADED =
            "app.crossword.yourealwaysbe.forkyzscanner.LANGUAGE_MODEL_DOWNLOADED"
    }

    private val DEFAULT_IMPORT_NAME = "downloaded.traineddata"

    override fun onReceive(context: Context, intent: Intent) {
        val manager =
            ContextCompat.getSystemService(context, DownloadManager::class.java)

        val id = intent.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, -1L)

        if (manager == null || id == -1L) return

        when (intent.action) {
            DownloadManager.ACTION_DOWNLOAD_COMPLETE -> {
                val uri = manager.getUriForDownloadedFile(id)
                CoroutineScope(Dispatchers.IO).launch {
                    try {
                        val filename = FileHandling.getFilename(
                            context,
                            uri,
                        ) ?: DEFAULT_IMPORT_NAME
                        FileHandling.openInputStream(context, uri).use { ins ->
                            CluesScanner.importLanguageModel(
                                context,
                                filename,
                                ins,
                            )
                        }
                        context.sendBroadcast(Intent(LANGUAGE_MODEL_DOWNLOADED))
                    } catch (e : IOException) {
                        // ignore
                    } finally {
                        FileHandling.deleteFile(context, uri)
                    }
                }
            }
            DownloadManager.ACTION_NOTIFICATION_CLICKED -> {
                // TODO
            }
        }
    }
}
