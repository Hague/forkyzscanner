
package app.crossword.yourealwaysbe.forkyzscanner.view

import android.content.Context
import android.util.AttributeSet
import android.util.TypedValue
import android.view.Gravity
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.core.view.ViewCompat
import androidx.core.widget.TextViewCompat
import androidx.gridlayout.widget.GridLayout
import androidx.lifecycle.findViewTreeLifecycleOwner

import app.crossword.yourealwaysbe.forkyzscanner.R
import app.crossword.yourealwaysbe.forkyzscanner.model.Puzzle.Box
import app.crossword.yourealwaysbe.forkyzscanner.model.Puzzle.Grid

class PuzzleGridView(context : Context, attrs : AttributeSet)
        : GridLayout(context, attrs) {

    fun interface Listener {
        fun onBoxClick(box : Box)
    }

    /**
     * Make the view show the grid
     *
     * @param grid the grid to show
     * @param listener for grid view events
     * @param editable whether the numbers are editable
     */
    fun setGrid(
        grid : Grid,
        listener : Listener,
    ) {
        removeAllViews()

        rowCount = grid.height
        columnCount = grid.width

        val lifecycleOwner = findViewTreeLifecycleOwner()

        grid.boxes.forEach { row ->
            row.forEach { box ->
                val boxView = TextView(context)

                boxView.gravity = Gravity.CENTER
                TextViewCompat.setAutoSizeTextTypeUniformWithConfiguration(
                    boxView, 5, 20, 1, TypedValue.COMPLEX_UNIT_SP
                )

                val span = GridLayout.spec(GridLayout.UNDEFINED, 1f)
                val gridParams =
                    GridLayout.LayoutParams(span, span).apply {
                        width = 0
                        height = 0
                    }

                boxView.setOnClickListener { listener.onBoxClick(box) }

                addView(boxView, gridParams)

                lifecycleOwner?.let { lifecycleOwner ->
                    box.isBlock.observe(
                        lifecycleOwner,
                        { isBlock ->
                            val bIsBlock = (isBlock ?: true)
                            val backgroundId =
                                if (bIsBlock) R.drawable.block_background
                                else R.drawable.cell_background
                            val textColorId =
                                if (bIsBlock) R.color.colorCell
                                else R.color.colorBlock
                            boxView.setBackground(
                                ContextCompat.getDrawable(context, backgroundId)
                            )
                            boxView.setTextColor(
                                ContextCompat.getColor(context, textColorId)
                            )
                            setContentDescription(boxView, box)
                        }
                    )

                    box.number.observe(
                        lifecycleOwner,
                        { number ->
                            boxView.text = (number?.toString() ?: "")
                            setContentDescription(boxView, box)
                        }
                    )
                }
            }
        }
    }

    private fun setContentDescription(view : TextView, box : Box) {
        val isBlock = box.isBlock.value == true
        val number = box.number.value ?: -1

        view.contentDescription = if (isBlock) {
                if (number > -1) {
                    context.getString(
                        R.string.content_desc_numbered_block,
                        number,
                    )
                } else {
                    context.getString(
                        R.string.content_desc_empty_block,
                    )
                }
            } else {
                if (number > -1) {
                    context.getString(
                        R.string.content_desc_numbered_cell,
                        number,
                    )
                } else {
                    context.getString(
                        R.string.content_desc_empty_cell,
                    )
                }
            }
    }
}
