
package app.crossword.yourealwaysbe.forkyzscanner

import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.constraintlayout.widget.ConstraintSet
import androidx.lifecycle.ViewModelProvider

import app.crossword.yourealwaysbe.forkyzscanner.databinding.GridBlocksFragmentBinding
import app.crossword.yourealwaysbe.forkyzscanner.model.Puzzle.Grid

// saved because finding these imports is hard
// import androidx.core.view.updateLayoutParams

public class GridBlocksFragment() : ScannerFragment() {
    private lateinit var viewModel : ForkyzScannerViewModel
    private lateinit var binding : GridBlocksFragmentBinding

    override fun onCreateView(
        inflater : LayoutInflater,
        container : ViewGroup?,
        savedInstanceState : Bundle?,
    ) : View {
        val provider = ViewModelProvider(requireActivity())
        viewModel = provider.get(ForkyzScannerViewModel::class.java)

        binding = GridBlocksFragmentBinding.inflate(inflater, container, false)

        setupView()
        setupObservers()

        return binding.root
    }

    private fun setupView() {
        binding.addTopButton.setOnClickListener { resizeGrid(-1, 0, 0, 0) }
        binding.deleteTopButton.setOnClickListener { resizeGrid(1, 0, 0, 0) }
        binding.addLeftButton.setOnClickListener { resizeGrid(0, -1, 0, 0) }
        binding.deleteLeftButton.setOnClickListener { resizeGrid(0, 1, 0, 0) }
        binding.addRightButton.setOnClickListener { resizeGrid(0, 0, 1, 0) }
        binding.deleteRightButton.setOnClickListener { resizeGrid(0, 0, -1, 0) }
        binding.addBottomButton.setOnClickListener { resizeGrid(0, 0, 0, 1) }
        binding.deleteBottomButton.setOnClickListener {
            resizeGrid(0, 0, 0, -1)
        }

        binding.scanCameraButton.setOnClickListener { getPhoto() }
        binding.scanImageButton.setOnClickListener { getImage() }
    }

    private fun resizeGrid(
        offsetTop : Int,
        offsetLeft : Int,
        offsetRight : Int,
        offsetBottom : Int,
    ) {
        viewModel.puzzle.value?.resizeGrid(
            offsetTop,
            offsetLeft,
            offsetRight,
            offsetBottom,
        )
    }

    protected override fun onImageGotten(imageUri : Uri) {
        viewModel.scanGridFromUri(imageUri)
    }

    private fun setupObservers() {
        viewModel.puzzle.observe(
            viewLifecycleOwner,
            { puzzle ->
                puzzle.grid.observe(
                    viewLifecycleOwner,
                    { grid -> onNewGrid(grid) }
                )
            }
        )
    }

    private fun onNewGrid(grid : Grid) {
        val set = ConstraintSet()
        set.clone(binding.constraintLayout)
        set.setDimensionRatio(
            R.id.grid_buttons,
            "" + grid.width + ":" + grid.height
        )
        set.applyTo(binding.constraintLayout)

        binding.gridButtons.setGrid(
            grid,
            { box ->
                val block = box.isBlock.value?.not() ?: true
                box.isBlock.value = block
            },
        )
    }
}
