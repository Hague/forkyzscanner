
package app.crossword.yourealwaysbe.forkyzscanner.androidversion

import android.content.Context
import android.content.pm.PackageManager.NameNotFoundException
import android.graphics.Bitmap
import android.net.ConnectivityManager
import android.net.Uri
import android.provider.MediaStore
import androidx.core.content.ContextCompat

import java.io.IOException

open class LollipopUtils() : AndroidVersionUtils() {
    @Suppress("deprecation")
    @Throws(IOException::class)
    override fun getBitmapFromUri(context : Context, uri : Uri) : Bitmap {
        return MediaStore.Images.Media.getBitmap(
            context.getContentResolver(),
            uri
        )
    }

    @Suppress("deprecation")
    override fun hasNetworkConnection(context : Context) : Boolean {
        val cm = ContextCompat.getSystemService(
            context, ConnectivityManager::class.java
        )
        val info = cm?.getActiveNetworkInfo()
        return info != null && info.isConnected()
    }

    @Suppress("deprecation")
    override fun getApplicationVersionName(context : Context) : String {
        try {
            val info = context.getPackageManager()
                .getPackageInfo(context.getPackageName(), 0)
            return info.versionName ?: ""
        } catch (e : NameNotFoundException) {
            return ""
        }
    }
}
