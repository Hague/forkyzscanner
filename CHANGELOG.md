# Forkyz Scanner Changelog

## Version 8

- Target Android 15.
- Add scan puzzle notes option.

## Version 7

- Update theme to match Forkyz 53.

## Version 6

- Fix settings crash.

## Version 5

- Theming tweaks.
- Target Android 14.

## Version 4

- Improve scanning of CrossHare puzzles.
- Warn users when clue numbering doesn't match grid.
- Don't escape / in IPuz output.

## Version 3

- Update theme to match Forkyz Version 43.

## Version 2

- Improve UI on small screens.
- Other small UI refinements.

## Version 1

First release.

- Scan grid from photo, image, or PDF, or enter manually.
- Adjust scanned numbering.
- Add clues or scan a photo, image, or PDF.
- Share puzzle with other apps.
- Configure languages in settings.


