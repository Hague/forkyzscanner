
package app.crossword.yourealwaysbe.forkyzscanner

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider

public class DummyFragment() : Fragment() {
    private lateinit var viewModel : ForkyzScannerViewModel

    override fun onCreateView(
        inflater : LayoutInflater,
        container : ViewGroup?,
        savedInstanceState : Bundle?
    ) : View {
        val view = inflater.inflate(
            R.layout.dummy_fragment, container, false
        )
        val provider = ViewModelProvider(requireActivity())
        viewModel = provider.get(ForkyzScannerViewModel::class.java)
        return view
    }
}
