
package app.crossword.yourealwaysbe.forkyzscanner.scanning

import android.graphics.Bitmap

import org.opencv.android.OpenCVLoader
import org.opencv.android.Utils
import org.opencv.core.Core
import org.opencv.core.Mat
import org.opencv.core.MatOfDouble
import org.opencv.core.MatOfPoint
import org.opencv.core.MatOfPoint2f
import org.opencv.core.Point
import org.opencv.core.Rect
import org.opencv.core.Size
import org.opencv.imgproc.Imgproc
import org.opencv.imgproc.Imgproc.GaussianBlur

import app.crossword.yourealwaysbe.forkyzscanner.model.Puzzle.Grid

/**
 * Extracts a crossword grid from a scanned image
 */
class GridScanner {
    private val DELTA_THRESHOLD = 15 // pixels apart, minimum

    /**
     * Scan a grid from the image
     *
     * Image assumed to be tightly cropped to the grid.
     *
     * Do not call on UI thread. Best use Dispatcher.Default as it will
     * be CPU heavy (after image is read).
     *
     * @param bitmap the bitmap of the image to extract from
     * @return the extracted grid
     */
    fun scanGridFromBitmap(bitmap : Bitmap) : Grid? {
        try {
            // Sticking with initDebug. The internet reports that initAsync prompts
            // for OpenCV manager installation, which is deprecated.
            if (!OpenCVLoader.initDebug())
                return null

            val mat = Mat()
            Utils.bitmapToMat(bitmap, mat, false)

            return scanGridFromMat(mat)
        } catch (e : Exception) {
            return null
        }
    }

    private fun scanGridFromMat(mat : Mat) : Grid {
        // TODO: massive tidy up of this experimental code
        val width = mat.cols()
        val height = mat.rows()
        val binary = preprocessMat(mat)
        val points = getCellPoints(binary)
        val deltas = getCellDeltas(points)
        val offsets = getStartOffsets(points, deltas, width, height)
        val dimensions = getGridDimensions(
            binary,
            offsets,
            deltas,
        )
        return readGrid(binary, offsets, deltas, dimensions)
    }

    /**
     * Returns the average colour in the defined square
     *
     * @param source the image
     * @param xc the centre x coord
     * @param yc the centre y coord
     * @param sqRadius the "radius" of the square
     */
    private fun localColor(
        source : Mat,
        xc : Int,
        yc : Int,
        sqRadius : Int
    ) : Double {
        val xmin = Math.max(0, xc - sqRadius)
        val xmax = Math.min(xc + sqRadius, source.cols())
        val ymin = Math.max(0, yc - sqRadius)
        val ymax = Math.min(yc + sqRadius, source.rows())

        val window = source.submat(Rect(xmin, ymin, xmax - xmin, ymax - ymin))

        val mean = MatOfDouble()
        val sigma = MatOfDouble()

        Core.meanStdDev(window, mean, sigma)

        return mean.get(0, 0)[0]
    }

    /**
     * Preprocess the image to aid scanning
     *
     * @param source the original image
     * @return the preprocessed image
     */
    private fun preprocessMat(source : Mat) : Mat {
        val gray = Mat()
        Imgproc.cvtColor(source, gray, Imgproc.COLOR_RGB2GRAY)

        // make black lines thicker by eroding white cells
        val kernel = Imgproc.getStructuringElement(
            Imgproc.MORPH_RECT, Size(5.0, 5.0)
        );
        val erode = Mat()
        Imgproc.erode(gray, erode, kernel)

        val blur = Mat()
        GaussianBlur(erode, blur, Size(1.0, 1.0), 0.0)

        val binary = Mat()
        Imgproc.adaptiveThreshold(
            blur,
            binary,
            255.0,
            Imgproc.ADAPTIVE_THRESH_MEAN_C,
            Imgproc.THRESH_BINARY,
            201,
            0.0,
        )

        return binary
    }

    /**
     * Get the centre points of all squares in the image
     *
     * @param binary the preprocessed image of the grid
     * @return a list of centre points
     */
    private fun getCellPoints(binary : Mat) : List<Point> {
        val contours = java.util.ArrayList<MatOfPoint>()
        val hierarchy = Mat()
        Imgproc.findContours(
            binary,
            contours,
            hierarchy,
            Imgproc.RETR_LIST,
            Imgproc.CHAIN_APPROX_SIMPLE,
        )

        // from contours, get centre points of squares
        val points : MutableList<Point> = mutableListOf()
        for (i in 0 until contours.size) {
            val contour = contours.get(i)

            val pivots = MatOfPoint2f()
            val contour2f = MatOfPoint2f()
            contour2f.fromList(contour.toList())
            Imgproc.approxPolyDP(contour2f, pivots, 5.0, true)

            if (pivots.rows() == 4) {
                val moments = Imgproc.moments(contour)
                val cX = moments.get_m10() / moments.get_m00()
                val cY = moments.get_m01() / moments.get_m00()
                points.add(Point(cX, cY))
            }
        }

        return points
    }

    /**
     * work out average min delta from centre of square to the next
     *
     * This gives a way of getting from one grid row/col to the next
     * accounting for slightly off-kilter grids
     */
    private fun getCellDeltas(cellPoints : List<Point>) : CellDeltas {
        // it's quadratic though, so probably want to limit the area
        // considered -- we don't really need **all** of the points to
        // estimate the deltas
        val minAcrossXDeltas : MutableList<Double> = mutableListOf()
        val minAcrossYDeltas : MutableList<Double> = mutableListOf()
        val minDownXDeltas : MutableList<Double> = mutableListOf()
        val minDownYDeltas : MutableList<Double> = mutableListOf()
        for (point in cellPoints) {
            var minDist = -1.0
            var minXDelta = -1.0
            var minYDelta = -1.0

            for (point2 in cellPoints) {
                if (!point.equals(point2)) {
                    var xdelta : Double = point.x - point2.x
                    var ydelta : Double = point.y - point2.y
                    if (Math.abs(xdelta) > Math.abs(ydelta) && xdelta < 0) {
                        xdelta = -xdelta
                        ydelta = -ydelta
                    } else if (ydelta < 0) {
                        xdelta = -xdelta
                        ydelta = -ydelta
                    }
                    val dist = Math.sqrt(xdelta * xdelta + ydelta * ydelta)
                    if (dist > DELTA_THRESHOLD) {
                        if (minDist < 0 || dist < minDist) {
                            minDist = dist
                            minXDelta = xdelta
                            minYDelta = ydelta
                        }
                    }
                }
            }

            // across or down
            if (minXDelta > minYDelta) {
                minAcrossXDeltas.add(minXDelta)
                minAcrossYDeltas.add(minYDelta)
            } else {
                minDownXDeltas.add(minXDelta)
                minDownYDeltas.add(minYDelta)
            }
        }

        minAcrossXDeltas.sort()
        minAcrossYDeltas.sort()
        minDownXDeltas.sort()
        minDownYDeltas.sort()

        var acrossXDelta = 0.0
        var acrossYDelta = 0.0
        var downXDelta = 0.0
        var downYDelta = 0.0

        if (minAcrossXDeltas.size > 0) {
            acrossXDelta = minAcrossXDeltas.get(minAcrossXDeltas.size / 2)
            acrossYDelta = minAcrossYDeltas.get(minAcrossYDeltas.size / 2)
        }
        if (minDownXDeltas.size > 0) {
            downXDelta = minDownXDeltas.get(minDownXDeltas.size / 2)
            downYDelta = minDownYDeltas.get(minDownYDeltas.size / 2)
        }
        if (minAcrossXDeltas.size == 0) {
            acrossXDelta = -downYDelta
            acrossYDelta = downXDelta
        }
        if (minDownXDeltas.size == 0) {
            downYDelta = acrossXDelta
            downXDelta = -acrossYDelta
        }

        return CellDeltas(
            acrossXDelta,
            acrossYDelta,
            downXDelta,
            downYDelta,
        )
    }

    /**
     * Try to estimate the top/left of the grid
     *
     * Returns estimated centre point.
     *
     * @param points list of centres of squares in grid
     * @param deltas across/down deltas
     * @param width number of cols in the grid image
     * @param height number of rows in grid image
     */
    private fun getStartOffsets(
        points : List<Point>,
        deltas : CellDeltas,
        width : Int,
        height : Int
    ) : Point {
        // now try to find where to start from by seeing how far off the
        // contour centres we are by applying the grid naively
        // (think of a better way!)
        val xOffsets : MutableList<Double> = mutableListOf()
        val yOffsets : MutableList<Double> = mutableListOf()

        var x = deltas.acrossXDelta / 2
        var y = deltas.downYDelta / 2
        while (y < height) {
            val xStart = x
            val yStart = y
            while (x < width) {
                var minDist = -1.0
                var minXOff = -1.0
                var minYOff = -1.0
                for (point in points) {
                    val xdelta = x - point.x
                    val ydelta = y - point.y
                    val dist = Math.sqrt(xdelta * xdelta + ydelta * ydelta)
                    if (minDist < 0 || dist < minDist) {
                        minDist = dist
                        minXOff = xdelta
                        minYOff = ydelta
                    }
                }

                xOffsets.add(minXOff)
                yOffsets.add(minYOff)

                x += deltas.acrossXDelta
                y += deltas.acrossYDelta
            }

            x = xStart + deltas.downXDelta
            y = yStart + deltas.downYDelta
        }

        xOffsets.sort()
        yOffsets.sort()
        x = deltas.acrossXDelta / 2
        y = deltas.downYDelta / 2
        x -= xOffsets.get(xOffsets.size / 2)
        y -= yOffsets.get(yOffsets.size / 2)
        while (x < 0) {
            x += deltas.acrossXDelta
            y += deltas.acrossYDelta
        }
        while (y < 0) {
            x += deltas.downXDelta
            y += deltas.downYDelta
        }

        return Point(x, y)
    }

    /**
     * Estimate how wide/high the grid is based on info
     *
     * @param binary the preprocessed image of the grid
     * @param offsets the centre of the top/left cell
     * @param deltas the across/down deltas
     * @return the estimated width/height of the grid
     */
    private fun getGridDimensions(
        binary : Mat,
        offsets : Point,
        deltas : CellDeltas,
    ) : Dimensions {
        var numCols = 0
        var numRows = 0

        var x = offsets.x
        var y = offsets.y

        while (y < binary.rows()) {
            numRows += 1

            val xStart = x
            val yStart = y

            var rowColCount = 0

            while (x < binary.cols()) {
                rowColCount += 1
                x += deltas.acrossXDelta
                y += deltas.acrossYDelta
            }

            numCols = Math.max(rowColCount, numCols)

            x = xStart + deltas.downXDelta
            y = yStart + deltas.downYDelta
        }

        return Dimensions(numCols, numRows)
    }

    /**
     * Read the grid from the image
     *
     * @param binary the preprocessed image
     * @param offsets the offset of the top/left cell centre
     * @param deltas the across/down deltas between cells
     * @param width the width of the grid to read
     * @param height the height of the grid to read
     * @return the grid extracted from binary with the above info
     */
    private fun readGrid(
        binary : Mat,
        offsets : Point,
        deltas : CellDeltas,
        dimensions : Dimensions
    ) : Grid {
        // false means not a block
        val binGrid = Array(dimensions.height) {
            Array(dimensions.width) { false }
        }

        var x = offsets.x
        var y = offsets.y
        var row = 0
        var col = 0

        // if we start in the wrong place, some rows/cols fall off the
        // image (if it's rotated a bit), ignore them
        var firstValidRow = -1
        var lastValidRow = 0
        var firstValidCol = 0
        var lastValidCol = -1

        while (y < binary.rows()) {
            val xStart = x
            val yStart = y

            while (x < binary.cols()) {
                if (x < 0) {
                    // fell off grid, set valid based on how far off we
                    // are, +1 because toInt rounds down
                    firstValidCol = (-x / deltas.acrossXDelta).toInt() + 1
                } else if (row < dimensions.height && col < dimensions.width) {
                    if (localColor(binary, x.toInt(), y.toInt(), 1) <= 127.0) {
                        binGrid[row][col] = true
                    }
                }

                x += deltas.acrossXDelta
                y += deltas.acrossYDelta

                if (y < 0 || y >= binary.rows()) break

                col += 1
            }

            if (x >= binary.cols() && y >= 0 && y < binary.rows()) {
                lastValidCol =
                    if (lastValidCol < 0) col - 1
                    else kotlin.math.min(lastValidCol, col - 1)
                if (firstValidRow < 0) firstValidRow = row
                lastValidRow = row
            }

            x = xStart + deltas.downXDelta
            y = yStart + deltas.downYDelta

            row += 1
            col = 0
        }

        // trim invalid bits before returning
        return makeGridFromMatrix(
            binGrid,
            firstValidRow,
            firstValidCol,
            lastValidRow,
            lastValidCol,
        )
    }

    /**
     * Makes grid from block matrix
     *
     * @return grid or Grid(0, 0) if invalid
     */
    private fun makeGridFromMatrix(
        binGrid : Array<Array<Boolean>>,
        firstValidRow : Int,
        firstValidCol : Int,
        lastValidRow : Int,
        lastValidCol : Int,
    ) : Grid {
        if ((firstValidRow < 0) || (firstValidCol < 0)) {
            return Grid(0, 0)
        }

        if (lastValidRow > binGrid.size) {
            return Grid(0, 0)
        }

        val width = lastValidCol - firstValidCol + 1
        val height = lastValidRow - firstValidRow + 1

        if (width <= 0 || height <= 0) {
            return Grid(0, 0)
        }

        val grid = Grid(width, height)

        for (row in firstValidRow..lastValidRow) {
            for (col in firstValidCol..lastValidCol) {
                if (col >= binGrid[row].size) {
                    return Grid(0, 0)
                } else {
                    val boxRow = row - firstValidRow
                    val boxCol = col - firstValidCol
                    grid.boxes[boxRow][boxCol]
                        .isBlock
                        .postValue(binGrid[row][col])
                }
            }
        }

        return grid
    }

    private class CellDeltas(
        val acrossXDelta : Double,
        val acrossYDelta : Double,
        val downXDelta : Double,
        val downYDelta : Double,
    )

    private class Dimensions(
        val width : Int,
        val height : Int
    )
}
