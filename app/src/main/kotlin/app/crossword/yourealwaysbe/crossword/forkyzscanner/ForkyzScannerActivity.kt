
package app.crossword.yourealwaysbe.forkyzscanner

import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ShareCompat
import androidx.core.content.FileProvider
import androidx.core.text.HtmlCompat
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.viewpager2.adapter.FragmentStateAdapter
import androidx.viewpager2.widget.ViewPager2
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.navigation.NavigationBarView
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.launch

import javax.inject.Inject

import dagger.hilt.android.AndroidEntryPoint

import app.crossword.yourealwaysbe.forkyzscanner.androidversion.versionUtils
import app.crossword.yourealwaysbe.forkyzscanner.databinding.ForkyzScannerActivityBinding
import app.crossword.yourealwaysbe.forkyzscanner.settings.PuzzleManager
import app.crossword.yourealwaysbe.forkyzscanner.settings.SettingsRepository

@AndroidEntryPoint
class ForkyzScannerActivity : ForkyzScannerBaseActivity() {

    @Inject
    lateinit var settings : SettingsRepository

    private val pagesPosMenuID = mapOf(
        0 to R.id.navigation_meta,
        1 to R.id.navigation_blocks,
        2 to R.id.navigation_numbers,
        3 to R.id.navigation_across_clues,
        4 to R.id.navigation_down_clues,
    )
    private val pagesMenuIDPos =
        pagesPosMenuID.entries.associate { (k, v) -> v to k }

    private lateinit var viewModel : ForkyzScannerViewModel
    private lateinit var binding : ForkyzScannerActivityBinding

    private lateinit var pagerAdapter : PagerAdapter

    override fun onCreate(savedInstanceState : Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ForkyzScannerActivityBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setSupportActionBar(binding.toolbar)
        setupSideInsets(binding.toolbar)
        setupBottomInsets(binding.content)
        setStatusBarElevation(binding.appBarLayout)

        versionCheck()
        initialiseUI()
    }

    override fun onPause() {
        super.onPause()
        viewModel.savePuzzle()
    }

    override fun onCreateOptionsMenu(menu : Menu) : Boolean {
        getMenuInflater().inflate(R.menu.forkyz_scanner_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item : MenuItem) : Boolean {
        when (item.itemId) {
            R.id.settings -> {
                startActivity(Intent(this, SettingsActivity::class.java))
                return true
            }
            R.id.share -> {
                sharePuzzle()
                return true
            }
        }

        return super.onOptionsItemSelected(item)
    }

    private fun initialiseUI() {
        val pager = binding.forkyzScannerPager
        val navigation = binding.forkyzScannerNavigation

        pagerAdapter = PagerAdapter(this)
        pager.adapter = pagerAdapter
        pager.registerOnPageChangeCallback(
            object : ViewPager2.OnPageChangeCallback() {
                override fun onPageSelected(position : Int) {
                    navigation.setSelectedItemId(pagesPosMenuID[position]!!)
                }
            }
        )

        navigation.setOnItemSelectedListener(
            object : NavigationBarView.OnItemSelectedListener {
                override fun onNavigationItemSelected(
                    item : MenuItem
                ) : Boolean {
                    val pos = pagesMenuIDPos[item.getItemId()]!!
                    // don't smooth scroll because it causes issues with big
                    // jumps
                    pager.setCurrentItem(pos, false)
                    return true
                }
            }
        )

        // Set up UI
        viewModel = ViewModelProvider(this).get(
            ForkyzScannerViewModel::class.java
        )
    }

    private fun sharePuzzle() {
        viewModel.puzzle.value?.let { puzzle ->
            lifecycleScope.launch {
                val fsaThis = this@ForkyzScannerActivity
                val file = PuzzleManager.savePuzzle(fsaThis, puzzle)
                val uri = FileProvider.getUriForFile(
                    fsaThis,
                    fsaThis.getApplicationContext().getPackageName() + ".provider",
                    file
                )
                val shareIntent = ShareCompat.IntentBuilder(fsaThis)
                    .setStream(uri)
                    .setType("application/json")
                    .setChooserTitle(getString(R.string.share))
                    .createChooserIntent()

                startActivity(shareIntent)
            }
        }
    }

    private fun versionCheck() {
        lifecycleScope.launch(Dispatchers.Default) {
            val currentVersion = versionUtils.getApplicationVersionName(
                this@ForkyzScannerActivity
            )
            val lastSeenVersion = settings.settingsFlow.first().lastSeenVersion
            if (lastSeenVersion.isEmpty()) {
                lifecycleScope.launch(Dispatchers.Main) {
                    FirstRunDialog()
                        .show(getSupportFragmentManager(), "FirstRunDialog")
                }
                settings.updateLastSeenVersion(currentVersion)
            } else if (currentVersion != lastSeenVersion) {
                lifecycleScope.launch(Dispatchers.Main) {
                    NewVersionDialog()
                        .show(getSupportFragmentManager(), "FirstRunDialog")
                }
                settings.updateLastSeenVersion(currentVersion)
            }
        }
    }

    private inner class PagerAdapter(fa : FragmentActivity)
            : FragmentStateAdapter(fa) {
        override fun getItemCount() : Int = pagesPosMenuID.size

        override fun createFragment(position : Int) : Fragment {
            return when (position) {
                0 -> MetadataFragment()
                1 -> GridBlocksFragment()
                2 -> GridNumbersFragment()
                3 -> CluesFragment.Across()
                4 -> CluesFragment.Down()
                else -> DummyFragment()
            }
        }
    }

    class FirstRunDialog() : DialogFragment() {
        override fun onCreateDialog(savedInstanceState : Bundle?) : Dialog {
            val activity = requireActivity()

            val builder = MaterialAlertDialogBuilder(activity)

            val version =
                versionUtils.getApplicationVersionName(activity)
            val title = getString(R.string.new_version_title, version)
            val message =
                HtmlCompat.fromHtml(getString(R.string.first_run_message), 0)

            builder.setTitle(title)
                .setMessage(message)
                .setPositiveButton(android.R.string.ok, null)
                .setNegativeButton(
                    R.string.settings,
                    { _, _ ->
                        activity.startActivity(
                            Intent(activity, SettingsActivity::class.java)
                        )
                    }
                )
            return builder.create()
        }
    }

    class NewVersionDialog() : DialogFragment() {
        override fun onCreateDialog(savedInstanceState : Bundle?) : Dialog {
            val activity = requireActivity()

            val builder = MaterialAlertDialogBuilder(activity)

            val version =
                versionUtils.getApplicationVersionName(activity)
            val title = getString(R.string.new_version_title, version)
            val message = getString(R.string.new_version_message)

            builder.setTitle(title)
                .setMessage(message)
                .setPositiveButton(android.R.string.ok, null)
                .setNegativeButton(
                    R.string.release_notes_button,
                    { _, _ ->
                        activity.startActivity(
                            Intent(activity, ReleaseNotesActivity::class.java)
                        )
                    }
                )
            return builder.create()
        }
    }

    private enum class PageType {
        META, GRID_BLOCKS, GRID_NUMBERS, ACROSS_CLUES, DOWN_CLUES
    }
}
