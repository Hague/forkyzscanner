
package app.crossword.yourealwaysbe.forkyzscanner.model

import androidx.arch.core.executor.testing.CountingTaskExecutorRule

import java.time.LocalDate
import java.util.concurrent.TimeUnit

import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotNull
import org.junit.Assert.assertNull
import org.junit.Rule
import org.junit.Test

import app.crossword.yourealwaysbe.forkyzscanner.model.Puzzle.ClueList

class PuzzleIPuzTest {
    // needed to make sure all postvalues are done before testing
    @get:Rule
    public val countingTaskExecutorRule = CountingTaskExecutorRule()

    private val puzzle1 = """{
        "version": "http://ipuz.org/v2",
        "kind": [ "http://ipuz.org/crossword#1" ],
        "title": "Test puzzle",
        "author": "Test author",
        "notes": "Test notes",
        "date": "02/01/2003",
        "dimensions": { "width": 3, "height": 2 },
        "puzzle": [
            [ 1, 2, "#" ],
            [ { "cell": 3 }, 0, { "cell": "#", "style": { label: "4" } } ]
        ],
        "clues": {
            "Across": [
                [ 1, "Test clue 1" ],
                [ "3", "Test clue 2" ],
            ],
            "Down": [
                { "number": 1, "clue": "Test clue 3" },
            ]
        }
    }"""

    @Test
    fun testBasicRead() {
        val puzzle = puzzleFromIPuz(puzzle1)
        waitPosts()
        assertIsPuzzle1(puzzle)
    }

    @Test
    fun testBasicWrite() {
        // assumes the read works ok
        // read, then test write and read back gives same puzzle!
        val puzzle = puzzleFromIPuz(puzzle1)
        waitPosts()

        assertNotNull(puzzle)
        val puzzleReread = puzzleFromIPuz(puzzleToIPuz(puzzle!!))
        waitPosts()

        assertIsPuzzle1(puzzleReread)
    }

    private fun assertIsPuzzle1(puzzle : Puzzle?) {
        assertNotNull(puzzle)

        assertEquals(puzzle?.title?.value, "Test puzzle")
        assertEquals(puzzle?.author?.value, "Test author")
        assertEquals(puzzle?.notes?.value, "Test notes")
        assertEquals(puzzle?.date?.value, LocalDate.of(2003, 2, 1))

        val grid = puzzle?.grid?.value
        assertNotNull(grid)
        assertEquals(grid?.width, 3)
        assertEquals(grid?.height, 2)

        grid?.boxes?.let { boxes ->
            assertEquals(boxes[0][0].number.value, 1)
            assertEquals(boxes[0][0].isBlock.value, false)
            assertEquals(boxes[0][1].number.value, 2)
            assertEquals(boxes[0][1].isBlock.value, false)
            assertNull(boxes[0][2].number.value)
            assertEquals(boxes[0][2].isBlock.value, true)
            assertEquals(boxes[1][0].number.value, 3)
            assertEquals(boxes[1][0].isBlock.value, false)
            assertNull(boxes[1][1].number.value)
            assertEquals(boxes[1][1].isBlock.value, false)
            assertEquals(boxes[1][2].number.value, 4)
            assertEquals(boxes[1][2].isBlock.value, true)
        }

        puzzle?.getClueList(ClueList.ACROSS)?.let { across ->
            assertEquals(across.size, 2)
            assertEquals(across[0].number.value, "1")
            assertEquals(across[0].hint.value, "Test clue 1")
            assertEquals(across[1].number.value, "3")
            assertEquals(across[1].hint.value, "Test clue 2")
        }
        puzzle?.getClueList(ClueList.DOWN)?.let { down ->
            assertEquals(down.size, 1)
            assertEquals(down[0].number.value, "1")
            assertEquals(down[0].hint.value, "Test clue 3")
        }
    }

    /**
     * Wait for all postValues to complete
     */
    private fun waitPosts() {
        countingTaskExecutorRule.drainTasks(3, TimeUnit.SECONDS)
    }
}
