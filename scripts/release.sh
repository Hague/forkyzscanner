#!/bin/fish

echo "Run from Forkyz Scanner root directory!"

if test (count $argv) -lt 1
    echo "Please pass the version name as the first argument. E.g. release.sh 40."
    exit
end

set fs_version $argv[1]

set changelog fastlane/metadata/android/en-US/changelogs/{$fs_version}.txt

nvim $changelog
git add $changelog
python scripts/make_release_notes.py
nvim app/build.gradle

echo "Ready to commit!"
echo "Don't forget to git add the new changelog."

git status
