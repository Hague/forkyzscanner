
package app.crossword.yourealwaysbe.forkyzscanner.util

import androidx.databinding.InverseMethod

// Based on
// https://stackoverflow.com/a/61836134/6882587
class DataBindingConverter {
    companion object {
        @InverseMethod("convertIntegerToString")
        @JvmStatic
        fun convertStringToInteger(value: String): Int? {
            return value.toIntOrNull()
        }

        @JvmStatic
        fun convertIntegerToString(value: Int?): String {
            return value?.toString() ?: ""
        }
    }
}
