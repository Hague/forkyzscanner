
package app.crossword.yourealwaysbe.forkyzscanner.settings

import android.content.Context

import java.io.File
import java.io.IOException
import kotlin.text.Charsets

import app.crossword.yourealwaysbe.forkyzscanner.model.Puzzle
import app.crossword.yourealwaysbe.forkyzscanner.model.puzzleFromIPuz
import app.crossword.yourealwaysbe.forkyzscanner.model.puzzleToIPuz

class PuzzleManager {
    companion object {
        private val PUZZLE_DIR_NAME = "puzzle_dir"
        private val PUZZLE_FILE_NAME = "puzzle.ipuz"
        private val CHARSET = Charsets.UTF_8

        @Throws(IOException::class)
        fun savePuzzle(context : Context, puzzle : Puzzle) : File {
            val file = getPuzzleFile(context)
            file.writeText(puzzleToIPuz(puzzle), CHARSET)
            return file
        }

        /**
         * Returns null if puzzle did not load
         */
        fun loadPuzzle(context : Context) : Puzzle? {
            try {
                return puzzleFromIPuz(getPuzzleFile(context).readText(CHARSET))
            } catch (e : IOException) {
                return null
            }
        }

        private fun getPuzzleFile(context : Context) : File {
            val dir = File(
                context.getFilesDir().getAbsolutePath(),
                PUZZLE_DIR_NAME
            )
            if (!dir.exists()) dir.mkdir()
            return File(dir, PUZZLE_FILE_NAME)
        }
    }
}
