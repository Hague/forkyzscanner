
package app.crossword.yourealwaysbe.forkyzscanner.androidversion

import android.annotation.TargetApi
import android.content.Context
import android.net.ConnectivityManager
import android.os.Build

@TargetApi(Build.VERSION_CODES.M)
open class MarshmallowUtils() : LollipopUtils() {
    override fun hasNetworkConnection(context : Context) : Boolean =
        context.getSystemService(ConnectivityManager::class.java)
            .getActiveNetwork() != null
}
