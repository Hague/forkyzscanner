
package app.crossword.yourealwaysbe.forkyzscanner

import android.app.Application
import android.net.Uri
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.asLiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.launch

import java.io.IOException
import javax.inject.Inject

import dagger.hilt.android.lifecycle.HiltViewModel

import app.crossword.yourealwaysbe.forkyzscanner.scanning.CluesScanner
import app.crossword.yourealwaysbe.forkyzscanner.settings.SettingsRepository
import app.crossword.yourealwaysbe.forkyzscanner.util.FileHandling

@HiltViewModel
class SettingsViewModel @Inject constructor(
    application : Application,
    private val settings : SettingsRepository,
) : AndroidViewModel(application) {
    private val DEFAULT_IMPORT_NAME = "imported.traineddata"

    val liveSettings = settings.settingsFlow.asLiveData()

    private val _languageModels = MutableLiveData<List<String>>(listOf())
    val languageModels : LiveData<List<String>>
        get() = _languageModels
    init {
        refreshLanguageModels()
    }

    fun setDynamicColors(value : Boolean) =
        viewModelScope.launch { settings.updateDynamicColors(value) }

    fun setSelectedLanguageModel(value : String) =
        viewModelScope.launch { settings.updateSelectedLanguageModel(value) }

    /**
     * Refresh language models and possibly selection
     *
     * If the selection is not in the list of language models, then
     * chooses any other one.
     */
    fun refreshLanguageModels() {
        viewModelScope.launch {
            val selectedModel =
                settings.settingsFlow.first().selectedLanguageModel
            val models =
                CluesScanner.getAvailableLanguageModels(getApplication())
            if (!(selectedModel in models)) {
                setSelectedLanguageModel(models.firstOrNull() ?: "")
            }
            _languageModels.postValue(models)
        }
    }

    fun importLanguageModel(uri : Uri) {
        viewModelScope.launch {
            try {
                val app : Application = getApplication()
                val filename =
                    FileHandling.getFilename(app, uri) ?: DEFAULT_IMPORT_NAME
                val input = FileHandling.openInputStream(app, uri)
                CluesScanner.importLanguageModel(app, filename, input)
                refreshLanguageModels()
            } catch (e : IOException) {
                // TODO: something
            }
        }
    }

    /**
     * Deletes the currently selected language model
     *
     * Selected as in what the settings backend thinks is selected.
     * Refreshes language models, which should change the selection to
     * something else, if available.
     */
    fun deleteSelectedLanguageModel() {
        viewModelScope.launch {
            try {
                val settingsValues = settings.settingsFlow.first()
                val app : Application = getApplication()
                CluesScanner.deleteLanguageModel(
                    app,
                    settingsValues.selectedLanguageModel,
                )
                refreshLanguageModels()
            } catch (e : IOException) {
                // TODO: something
            }
        }
    }
}
