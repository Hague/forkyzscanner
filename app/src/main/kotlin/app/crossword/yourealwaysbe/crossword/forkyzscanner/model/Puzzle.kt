
package app.crossword.yourealwaysbe.forkyzscanner.model

import androidx.lifecycle.MutableLiveData

import java.time.LocalDate
import java.util.EnumMap

import app.crossword.yourealwaysbe.forkyzscanner.util.buildWeakSet

class Puzzle() {
    /**
     * The types of list available
     */
    enum class ClueList { ACROSS, DOWN }

    /**
     * Clue list listener events
     *
     * Indices are compatible with RecylerView.Adapter notifications and
     * in sequence. E.g. [ deleted(i), deleted(i) ] means the ith was
     * deleted, then the new ith was deleted.
     */
    sealed class ClueListEvent()
    data class ClueChanged(val index : Int) : ClueListEvent()
    data class ClueAdded(val index : Int) : ClueListEvent()
    data class ClueMoved(val src : Int, val dst : Int) : ClueListEvent()
    data class ClueDeleted(val index : Int) : ClueListEvent()

    interface ClueListListener {
        /**
         * When the clue list changes
         *
         * @param list the list changed
         * @param newList the new complete value of the list
         * @param changed a list of changes since the last update
         */
        fun onClueListChange(
            list : ClueList,
            newList : List<Clue>,
            changed : List<ClueListEvent>,
        )
    }

    class Box(val row : Int, val col : Int) {
        val isBlock = MutableLiveData<Boolean>()
        val number = MutableLiveData<Int?>()
        init {
            // needed in case Box created off main thread
            isBlock.postValue(false)
        }

        constructor (
            newRow : Int,
            newCol : Int,
            copy : Box,
        ) : this(newRow, newCol) {
            isBlock.postValue(copy.isBlock.getValue())
            number.postValue(copy.number.getValue())
        }
    }

    class Grid {
        val width : Int
        val height : Int

        private val _boxes : Array<Array<Box>>
        val boxes : Array<out Array<out Box>>
            get() = _boxes

        constructor(boxes : Array<Array<Box>>) {
            this.height = boxes.size
            this.width = if (this.height == 0) 0 else boxes[1].size
            this._boxes = boxes

            for (row in boxes) {
                if (row.size != this.width) {
                    throw IllegalArgumentException(
                        "All rows in grid should be same length"
                    )
                }
            }
        }

        /**
         * Copy the grid, adding or removing columns$
         *
         * Will only work on main thread as live data may not be up to date
         * otherwise.
         *
         * offsetX is used to add/del rows/cols.
         * E.g. if offsetLeft is -1, one column is added to the left.
         * If offsetLeft is 1, one column is removed from the left.
         * If offsetRight is 1, one column is added to the right.
         * If offsetRight is -1, one column is removed from the right.
         */
        constructor (
            copy : Grid,
            offsetTop : Int,
            offsetLeft : Int,
            offsetRight : Int,
            offsetBottom : Int,
        ) {
            this.width = copy.width + offsetRight - offsetLeft
            this.height = copy.height + offsetBottom - offsetTop

            val limitRow = copy.height + kotlin.math.min(0, offsetBottom)
            val limitCol = copy.width + kotlin.math.min(0, offsetRight)

            this._boxes = Array(height) { row ->
                val copyRow = row + offsetTop
                Array(width) { col ->
                    val copyCol = col + offsetLeft

                    val copyBox =
                        copyRow >= 0 && copyRow < limitRow &&
                        copyCol >= 0 && copyCol < limitCol

                    if (copyBox) {
                        Box(row, col, copy.boxes[copyRow][copyCol])
                    } else {
                        Box(row, col)
                    }
                }
            }
        }

        constructor(width : Int, height : Int) {
            this.width = width
            this.height = height
            this._boxes = Array(height) { row ->
                Array(width) { col -> Box(row, col) }
            }
        }

        /**
         * Replace grid numbers with standard system
         */
        fun overwriteNumbersStandard() {
            var nextClueNumber = 1
            _boxes.forEach { row ->
                row.forEach { box ->
                    val numbered = isCell(box) && (
                        (!hasCellLeft(box) && hasCellRight(box)) ||
                        (!hasCellAbove(box) && hasCellBelow(box))
                    )
                    box.number.postValue(
                        if (numbered) nextClueNumber++ else null
                    )
                }
            }
        }

        /**
         * Check if number appears in grid in the specified direction
         */
        fun hasNumber(number : Int, list : ClueList) : Boolean {
            _boxes.forEach { row ->
                row.forEach { box ->
                    if (isCell(box) && box.number.value == number) {
                        if (isStartClue(box, list))
                            return true
                    }
                }
            }
            return false
        }

        /**
         * Return a fresh mutable set of all numbers in given direction
         */
        fun getNumbers(list : ClueList) : MutableSet<Int> {
            val numbers = mutableSetOf<Int>()
            _boxes.forEach { row ->
                row.forEach { box ->
                    if (isCell(box)) {
                        box.number.value?.let { number ->
                            if (isStartClue(box, list))
                                numbers.add(number)
                        }
                    }
                }
            }
            return numbers
        }

        private fun isCell(box : Box) : Boolean = hasCellDelta(box, 0, 0)
        private fun hasCellAbove(box : Box) : Boolean = hasCellDelta(box, -1, 0)
        private fun hasCellBelow(box : Box) : Boolean = hasCellDelta(box, 1, 0)
        private fun hasCellLeft(box : Box) : Boolean = hasCellDelta(box, 0, -1)
        private fun hasCellRight(box : Box) : Boolean = hasCellDelta(box, 0, 1)

        private fun hasCellDelta(
            box : Box,
            rowDelta : Int,
            colDelta : Int,
        ) : Boolean {
            val deltaBox = checkedGetBox(
                box.row + rowDelta,
                box.col + colDelta,
            )
            if (deltaBox == null) return false
            else return !(deltaBox.isBlock.getValue() ?: true)
        }

        private fun checkedGetBox(row : Int, col : Int) : Box? {
            if (row < 0 || row >= _boxes.size) return null
            if (col < 0 || col >= _boxes[row].size) return null
            return _boxes[row][col]
        }

        /**
         * True if box is the start of a clue in direction
         */
        private fun isStartClue(box : Box, list : ClueList) : Boolean {
            return when (list) {
                ClueList.ACROSS -> (
                    isCell(box) && hasCellRight(box) && !hasCellLeft(box)
                )
                ClueList.DOWN -> {
                    isCell(box) && hasCellBelow(box) && !hasCellAbove(box)
                }
            }
        }
    }

    /**
     * Use "" for no number
     */
    class Clue(number : String = "", hint : String = "") {
        val number = MutableLiveData<String>()
        val hint = MutableLiveData<String>()
        init {
            // in case Clue created off main thread
            this.number.postValue(number)
            this.hint.postValue(hint)
        }
    }

    private val clueLists =
        EnumMap<ClueList, MutableList<Clue>>(
            ClueList::class.java
        )
    init {
        clueLists[ClueList.ACROSS] = mutableListOf<Clue>()
        clueLists[ClueList.DOWN] = mutableListOf<Clue>()
    }

    private val clueListListeners =
        EnumMap<ClueList, MutableSet<ClueListListener>>(
            ClueList::class.java
        )

    val title = MutableLiveData<String>()
    val author = MutableLiveData<String>()
    val date = MutableLiveData<LocalDate>()
    val notes = MutableLiveData<String>()

    init {
        title.postValue("")
        author.postValue("")
        date.postValue(LocalDate.now())
        notes.postValue("")
    }

    /**
     * grid[row][col] is the Box at (row, col)
     */
    val grid = MutableLiveData<Grid>()

    constructor(width : Int, height : Int) : this() {
        grid.postValue(Grid(width, height))
    }

    constructor(boxes : Array<Array<Box>>) : this() {
        grid.postValue(Grid(boxes))
    }

    constructor(grid : Grid) : this() {
        this.grid.postValue(grid)
    }

    fun getClueList(list : ClueList) : List<Clue> {
        return clueLists[list]!!
    }

    /**
     * Check if number is in puzzle grid in given direction/list
     */
    fun hasNumber(number : Int, list : ClueList) : Boolean {
        return grid.value?.hasNumber(number, list) ?: false
    }

    /**
     * Get numbers in direction from grid or empty if no grid
     */
    fun getNumbers(list : ClueList) : MutableSet<Int> {
        return grid.value?.getNumbers(list) ?: mutableSetOf()
    }

    /**
     * Resize the grid, adding or removing columns
     *
     * offsetX is used to add/del rows/cols.
     * E.g. if offsetLeft is -1, one column is added to the left.
     * If offsetLeft is 1, one column is removed from the left.
     * If offsetRight is 1, one column is added to the right.
     * If offsetRight is -1, one column is removed from the right.
     */
    fun resizeGrid(
        offsetTop : Int,
        offsetLeft : Int,
        offsetRight : Int,
        offsetBottom : Int
    ) {
        grid.getValue()?.let { gridVal ->
            val newGrid = Grid(
                gridVal,
                offsetTop,
                offsetLeft,
                offsetRight,
                offsetBottom,
            )
            grid.postValue(newGrid)
        }
    }

    fun addClues(list : ClueList, newClues : List<Clue>) {
        val clueList = clueLists[list]!!
        val start = clueList.size

        clueList.addAll(newClues)

        val additions = (0..(newClues.size - 1)).map {
            ClueAdded(it + start)
        }
        notifyClueListListeners(list, additions)
    }

    fun addClue(list : ClueList, newClue : Clue) {
        val clueList = clueLists[list]!!
        clueList.add(newClue)
        notifyClueListListeners(list, listOf(ClueAdded(clueList.size - 1)))
    }

    /**
     * Split the clue at index in list at position in hint
     *
     * Creates new numberless clue below in the list with all of the hint after
     * position.
     */
    fun splitClue(list : ClueList, index : Int, position : Int) {
        clueLists[list]?.let { clueList ->
            if (index < 0 || index >= clueList.size) return

            val clue = clueList[index]
            clue.hint.value?.let { hint ->
                if (position < 0 || position > hint.length) return

                val hintLeft = hint.substring(0, position).trim()
                val hintRight = hint.substring(position).trim()

                val clueLeft = Clue(clue.number.value ?: "", hintLeft)
                val clueRight = Clue("", hintRight)

                val indexPlus = kotlin.math.min(index + 1, clueList.size)

                val newList = mutableListOf<Clue>()
                with(newList) {
                    addAll(clueList.subList(0, index))
                    add(clueLeft)
                    add(clueRight)
                    addAll(clueList.subList(indexPlus, clueList.size))
                }

                clueLists[list] = newList
                notifyClueListListeners(
                    list,
                    listOf(ClueChanged(index), ClueAdded(index + 1)),
                )
            }
        }
    }

    /**
     * Delete the clues at the given indices
     */
    fun deleteClues(list : ClueList, indices : Set<Int>) {
        clueLists[list]?.let { clueList ->
            val newList = mutableListOf<Clue>()
            val changed = mutableListOf<ClueListEvent>()

            for ((index, clue) in clueList.withIndex()) {
                if (index in indices) {
                    changed.add(ClueDeleted(newList.size))
                } else {
                    newList.add(clue)
                }
            }

            clueLists[list] = newList
            notifyClueListListeners(list, changed)
        }
    }

    /**
     * Move clues at given indices up
     */
    fun moveCluesUp(list : ClueList, indices : Set<Int>) {
        if (0 in indices) return

        clueLists[list]?.let { clueList ->
            val changed = mutableListOf<ClueListEvent>()

            for ((index, clue) in clueList.withIndex()) {
                // index guaranteed > 0 by entry test of function
                if (index in indices) {
                    val clueBefore = clueList[index - 1]
                    clueList[index - 1] = clue
                    clueList[index] = clueBefore
                    changed.add(ClueMoved(index, index - 1))
                }
            }

            notifyClueListListeners(list, changed)
        }
    }

    /**
     * Move clues at given indices down
     */
    fun moveCluesDown(list : ClueList, indices : Set<Int>) {
        clueLists[list]?.let { clueList ->
            if ((clueList.size - 1) in indices) return

            val changed = mutableListOf<ClueListEvent>()

            for (index in (clueList.size - 1) downTo 0) {
                // index guaranteed < len - 1 by test above loop
                if (index in indices) {
                    val clueAfter = clueList[index + 1]
                    clueList[index + 1] = clueList[index]
                    clueList[index] = clueAfter
                    changed.add(ClueMoved(index, index + 1))
                }
            }

            notifyClueListListeners(list, changed)
        }
    }

    fun addClueListListener(list : ClueList, listener : ClueListListener) {
        if (!(list in clueListListeners)) {
            clueListListeners.put(list, buildWeakSet<ClueListListener>())
        }
        clueListListeners.get(list)?.add(listener)
    }

    fun removeClueListListener(list : ClueList, listener : ClueListListener) {
        clueListListeners.get(list)?.let { it.remove(listener) }
    }

    private fun notifyClueListListeners(
        list : ClueList,
        changed : List<ClueListEvent>,
    ) {
        if (changed.size == 0) return

        clueListListeners.get(list)?.let { listeners ->
            clueLists[list]?.let { clues ->
                listeners.forEach { it.onClueListChange(list, clues, changed) }
            }
        }
    }
}
