
package app.crossword.yourealwaysbe.forkyzscanner.model

import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.time.format.DateTimeParseException
import java.util.Locale

import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import org.json.JSONStringer
import org.json.JSONTokener

private val DATE_FORMATTER =
    DateTimeFormatter.ofPattern("MM/dd/yyyy", Locale.US)

private val BLOCK = "#"
private val EMPTY = "0"

/**
 * Write puzzle to an IPuz string
 */
fun puzzleToIPuz(puzzle : Puzzle) : String {
    val ipuz = JSONStringer()
    ipuz.`object`()
    writeHeader(puzzle, ipuz)
    writeGrid(puzzle, ipuz)
    writeClues(puzzle, ipuz)
    ipuz.endObject()
    // bit hacky but JSONStringer is hardcoded to encode / as \/, which
    // confuses some IPuz parsers that expect e.g.
    // http://ipuz.org/crossword#1 to not have escaped /s.
    // e.g. https://stackoverflow.com/q/16563579
    // and https://android.googlesource.com/platform/libcore/+/android-4.2.2_r1/json/src/main/java/org/json/JSONStringer.java
    return ipuz.toString().replace("\\/", "/")
}

/**
 * Read puzzle from an IPuz string
 *
 * @return the puzzle or null if failed
 */
fun puzzleFromIPuz(ipuzString : String) : Puzzle? {
    try {
        val ipuz = JSONTokener(ipuzString).nextValue() as JSONObject
        val puzzle = readGrid(ipuz)
        if (puzzle != null) {
            readHeader(ipuz, puzzle)
            readClues(ipuz, puzzle)
        }
        return puzzle
    } catch (e : JSONException) {
        return null
    }
}

private fun writeHeader(puzzle : Puzzle, ipuz : JSONStringer) {
    writeKeyValue("version", "http://ipuz.org/v2", ipuz)
    ipuz.key("kind")
        .array()
        .value("http://ipuz.org/crossword#1")
        .endArray()
    writeKeyValue("title", puzzle.title.value, ipuz)
    writeKeyValue("author", puzzle.author.value, ipuz)
    writeKeyValue("notes", puzzle.notes.value, ipuz)

    val date = puzzle.date.value ?: LocalDate.now()
    writeKeyValue("date", DATE_FORMATTER.format(date), ipuz)
}

private fun writeGrid(puzzle : Puzzle, ipuz : JSONStringer) {
    puzzle.grid.value?.let { grid ->
        ipuz.key("dimensions")
            .`object`()
            .key("width")
            .value(grid.width)
            .key("height")
            .value(grid.height)
            .endObject()

        ipuz.key("puzzle").array()
        for (row in grid.boxes) {
            ipuz.array()
            for (box in row) {
                writeBox(box, ipuz)
            }
            ipuz.endArray()
        }
        ipuz.endArray()
    }
}

private fun writeBox(box : Puzzle.Box, ipuz : JSONStringer) {
    val block = box.isBlock.value ?: false
    val number = box.number.value
    if (block && number != null) {
        ipuz.`object`()
            .key("cell")
            .value(BLOCK)
            .key("style")
            .`object`()
            .key("label")
            .value(number.toString())
            .endObject()
            .endObject()
    } else if (block) {
        ipuz.value(BLOCK)
    } else {
        ipuz.value(number?.toString() ?: EMPTY)
    }
}

private fun writeClues(puzzle : Puzzle, ipuz : JSONStringer) {
    ipuz.key("clues").`object`()
    writeClueList(
        "Across",
        puzzle.getClueList(Puzzle.ClueList.ACROSS),
        ipuz
    )
    writeClueList(
        "Down",
        puzzle.getClueList(Puzzle.ClueList.DOWN),
        ipuz
    )
    ipuz.endObject()
}

private fun writeClueList(
    name : String,
    clues : List<Puzzle.Clue>,
    ipuz : JSONStringer
) {
    ipuz.key(name).array()
    for (clue in clues) {
        ipuz.array()
            .value(clue.number.value ?: "")
            .value(clue.hint.value ?: "")
            .endArray()
    }
    ipuz.endArray()
}

private fun writeKeyValue(
    key : String,
    value : String?,
    ipuz : JSONStringer,
) {
    ipuz.key(key).value(value ?: "")
}

@Throws(JSONException::class)
private fun readHeader(ipuz : JSONObject, puzzle : Puzzle) {
    puzzle.title.postValue(ipuz.optString("title", ""))
    puzzle.author.postValue(ipuz.optString("author", ""))
    puzzle.title.postValue(ipuz.optString("title", ""))
    puzzle.title.postValue(ipuz.optString("title", ""))
    puzzle.notes.postValue(ipuz.optString("notes", ""))

    var date = LocalDate.now()
    try {
        date = LocalDate.parse(ipuz.optString("date", ""), DATE_FORMATTER)
    } catch (e : DateTimeParseException) {
        // stick with date = now
    }
    puzzle.date.postValue(date)
}

/**
 * Returns the puzzle with a grid read from the ipuz
 *
 * Null or JSONException if didn't have good info
 */
@Throws(JSONException::class)
private fun readGrid(ipuz : JSONObject) : Puzzle? {
    ipuz.optJSONArray("puzzle")?.let { grid ->
        ipuz.optJSONObject("dimensions")?.let { dims ->
            val width = dims.optInt("width", 0)
            val height = dims.optInt("height", 0)
            val boxes = Array(height) { row ->
                val gridRow = grid.getJSONArray(row)
                Array(width) { col ->
                    readBox(gridRow.get(col), row, col)
                }
            }
            return Puzzle(boxes)
        }
    }
    return null
}

@Throws(JSONException::class)
private fun readBox(jsonBox : Any, row : Int, col : Int) : Puzzle.Box {
    val box = Puzzle.Box(row, col)

    // just count everything as strings
    val boxObj = if (jsonBox is Int) jsonBox.toString() else jsonBox

    if (boxObj is String) {
        if (boxObj == BLOCK) {
            box.isBlock.postValue(true)
        } else if (boxObj != EMPTY) {
            try {
                box.number.postValue(boxObj.toInt())
            } catch (e : NumberFormatException) {
                // ignore
            }
        }
    } else if (boxObj is JSONObject) {
        val cell = boxObj.optString("cell")
        if (cell == BLOCK) {
            box.isBlock.postValue(true)
            boxObj.optJSONObject("style")?.let { style ->
                try {
                    val number = style.optString("label", "").toInt()
                    box.number.postValue(number)
                } catch (e : NumberFormatException) {
                    // ignore
                }
            }
        } else if (cell != EMPTY) {
             try {
                box.number.postValue(cell.toInt())
            } catch (e : NumberFormatException) {
                // ignore
            }
        }
    }

    return box
}

@Throws(JSONException::class)
private fun readClues(ipuz : JSONObject, puzzle : Puzzle) {
    ipuz.optJSONObject("clues")?.let { clues ->
        clues.optJSONArray("Across")?.let { across ->
            val acrossClues = readClueList(across)
            puzzle.addClues(Puzzle.ClueList.ACROSS, acrossClues)
        }
        clues.optJSONArray("Down")?.let { down ->
            val downClues = readClueList(down)
            puzzle.addClues(Puzzle.ClueList.DOWN, downClues)
        }
    }
}

@Throws(JSONException::class)
private fun readClueList(clues : JSONArray?) : List<Puzzle.Clue> {
    if (clues == null) return listOf<Puzzle.Clue>()

    val clueList = mutableListOf<Puzzle.Clue>()

    for (i in 0..(clues.length() - 1)) {
        clues.opt(i)?.let { clue ->
            readClue(clue)?.let { clueList.add(it) }
        }
    }

    return clueList
}

/**
 * Tries to read a clue from clueObj
 *
 * Returns null or throws exception if fails
 */
@Throws(JSONException::class)
private fun readClue(clueObj : Any) : Puzzle.Clue? {
    if (clueObj is JSONArray) {
        val number = clueObj.optString(0, "")
        val hint = clueObj.optString(1, "")
        return Puzzle.Clue(number, hint)
    } else if (clueObj is JSONObject) {
        val number = clueObj.optString("number", "")
        val hint = clueObj.optString("clue", "")
        return Puzzle.Clue(number, hint)
    }
    return null
}
