#!/bin/python

# Convencience script for generating release.html and changes.md from fastlane
# changelogs.
#
# To be run from project root.

import glob
import os
import shutil
import tempfile

string_file="app/src/main/res/values/strings.xml"
changes_md="CHANGELOG.md"

# Make Release Notes

def version_number(filename: str) -> int:
    start = filename.rfind(os.path.sep) + 1
    end = -len(".txt")
    return int(filename[start:end])

release_notes = ""

changelogs = glob.glob("fastlane/metadata/android/en-US/changelogs/*")
changelogs.sort(key=lambda file: -version_number(file))

for file in changelogs:
    release_notes += f"<h2>Version {version_number(file)}</h2>"
    with open(file) as changelog:
        for line in changelog:
            if line.startswith("-"):
                line = line[1:]
            line = line.strip()
            line = line.replace("'", "\\'")
            if len(line) > 0:
                release_notes += f"• {line}<br>"

# Replace strings.xml with new notes

tmp_fd, tmp_path = tempfile.mkstemp()
try:
    with os.fdopen(tmp_fd, 'w') as tmp:
        with open(string_file) as strings:
            for line in strings:
                if "<string name=\"release_notes\"" in line:
                    tmp.write(
                        "    <string name=\"release_notes\"><![CDATA[" +
                        release_notes +
                        "]]></string>\n"
                    )
                else:
                    tmp.write(line)
    shutil.move(tmp_path, string_file)
finally:
    if os.path.exists(tmp_path):
        os.remove(tmp_path)

# Make CHANGELOG.md

with open(changes_md, "w") as changes_md:
    changes_md.write("# Forkyz Scanner Changelog\n\n")

    changelogs = glob.glob("fastlane/metadata/android/en-US/changelogs/*")
    changelogs.sort(key=lambda file: -version_number(file))

    for file in changelogs:
        changes_md.write(f"## Version {version_number(file)}\n\n")
        with open(file) as changelog:
            for line in changelog:
                changes_md.write(line)
        changes_md.write("\n")
