
package app.crossword.yourealwaysbe.forkyzscanner.androidversion

import android.annotation.TargetApi
import android.content.Context
import android.graphics.Bitmap
import android.graphics.ImageDecoder
import android.net.Uri
import android.os.Build

import java.io.IOException

@TargetApi(Build.VERSION_CODES.P)
open class PieUtils() : MarshmallowUtils() {
    @Throws(IOException::class)
    override fun getBitmapFromUri(context : Context, uri : Uri) : Bitmap {
        val source = ImageDecoder.createSource(
            context.getContentResolver(), uri
        )
        return ImageDecoder.decodeBitmap(source) { decoder, _, _ ->
            decoder.isMutableRequired = true
        }
    }
}
