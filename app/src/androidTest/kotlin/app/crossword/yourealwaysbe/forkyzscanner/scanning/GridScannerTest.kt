
package app.crossword.yourealwaysbe.forkyzscanner.scanning

import android.graphics.BitmapFactory
import androidx.arch.core.executor.testing.CountingTaskExecutorRule

import java.util.concurrent.TimeUnit

import org.junit.Assert.assertEquals
import org.junit.Rule
import org.junit.Test

import app.crossword.yourealwaysbe.forkyzscanner.model.Puzzle.Grid

class GridScannerTest {
    // needed to make sure all postvalues are done before testing
    @get:Rule
    public val countingTaskExecutorRule = CountingTaskExecutorRule()

    @Test
    fun testGrid1() {
        val grid = getGrid("/grid1.png")

        countingTaskExecutorRule.drainTasks(3, TimeUnit.SECONDS)

        assertEquals(grid.width, 15)
        assertEquals(grid.height, 15)

        for (col in 0..14)
            assertEquals(grid.boxes[0][col].isBlock.getValue(), col == 5)
        val cellCols = listOf(0, 2, 6, 8, 12, 14).toSet()
        for (col in 0..14)
            assertEquals(
                grid.boxes[7][col].isBlock.getValue(),
                !cellCols.contains(col)
            )
        for (col in 0..14)
            assertEquals(grid.boxes[14][col].isBlock.getValue(), col == 9)
    }

    @Test
    fun testGrid2() {
        val grid = getGrid("/grid2.png")

        countingTaskExecutorRule.drainTasks(3, TimeUnit.SECONDS)

        assertEquals(grid.width, 15)
        assertEquals(grid.height, 15)

        for (col in 0..14)
            assertEquals(grid.boxes[0][col].isBlock.getValue(), col % 2 == 0)
        for (col in 0..14)
            assertEquals(grid.boxes[7][col].isBlock.getValue(), col == 7)
        for (col in 0..14)
            assertEquals(grid.boxes[14][col].isBlock.getValue(), col % 2 == 0)
    }

    /**
     * Test issue from GitLab
     *
     * Thanks hugleo
     */
    @Test
    fun testGridIssue1() {
        val grid = getGrid("/issue1.png")

        countingTaskExecutorRule.drainTasks(3, TimeUnit.SECONDS)

        val c = false
        val b = true

        assertIsGrid(
            grid,
            arrayOf(
                arrayOf(c, c, c, c, c, c, c, c, c, c),
                arrayOf(b, b, c, c, b, b, c, c, b, b),
                arrayOf(c, c, b, c, c, c, c, b, c, c),
                arrayOf(c, c, c, b, c, c, b, c, c, c),
                arrayOf(b, c, c, c, b, b, c, c, c, b),
                arrayOf(b, c, b, c, b, b, c, b, c, b),
                arrayOf(c, c, c, b, c, c, b, c, c, c),
                arrayOf(c, c, b, c, c, c, c, b, c, c),
                arrayOf(b, b, c, c, b, b, c, c, b, b),
                arrayOf(c, c, c, c, c, c, c, c, c, c),
            )
        )
    }

    private fun getGrid(path : String) : Grid {
        val bitmap = BitmapFactory.decodeStream(
            GridScannerTest::class.java.getResourceAsStream(path)
        )
        return GridScanner().scanGridFromBitmap(bitmap)!!
    }

    /**
     * Assert grid matches boxes
     *
     * @param grid the grid
     * @param boxes 2D array of booleans, false for cell, true for
     * block, rows then cols
     */
    private fun assertIsGrid(grid : Grid, boxes : Array<Array<Boolean>>) {
        if (boxes.size == 0) {
            assertEquals(grid.height, 0)
            return
        }

        val height = boxes.size
        val width = boxes[0].size

        assertEquals(grid.height, height)
        assertEquals(grid.width, width)

        for (row in 0..height - 1) {
            for (col in 0..width - 1) {
                assertEquals(
                    grid.boxes[row][col].isBlock.getValue(),
                    boxes[row][col]
                )
            }
        }
    }
}
