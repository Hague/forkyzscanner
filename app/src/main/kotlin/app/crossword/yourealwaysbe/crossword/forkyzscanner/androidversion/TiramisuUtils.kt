
package app.crossword.yourealwaysbe.forkyzscanner.androidversion

import android.annotation.TargetApi
import android.content.Context
import android.content.pm.PackageManager.NameNotFoundException
import android.content.pm.PackageManager.PackageInfoFlags
import android.os.Build

@TargetApi(Build.VERSION_CODES.TIRAMISU)
open class TiramisuUtils() : PieUtils() {
    override fun getApplicationVersionName(context : Context) : String {
        try {
            val info = context.getPackageManager()
                .getPackageInfo(context.getPackageName(), PackageInfoFlags.of(0))
            return info.versionName ?: ""
        } catch (e : NameNotFoundException) {
            return ""
        }
    }
}
