
package app.crossword.yourealwaysbe.forkyzscanner.settings

import androidx.datastore.core.DataStore
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.catch

import java.io.IOException

class SettingsRepository(
    val settingsStore: DataStore<Settings>
) {
    val settingsFlow: Flow<Settings> = settingsStore.data
        .catch { exception ->
            if (exception is IOException) {
                emit(Settings.getDefaultInstance())
            } else {
                throw exception
            }
        }

    suspend fun updateDynamicColors(value : Boolean) {
        settingsStore.updateData { currentPreferences ->
            currentPreferences.toBuilder().setDynamicColors(value).build()
        }
    }

    suspend fun updateSelectedLanguageModel(value : String) {
        settingsStore.updateData { currentPreferences ->
            currentPreferences.toBuilder()
                .setSelectedLanguageModel(value)
                .build()
        }
    }

    suspend fun updateLastSeenVersion(value : String) {
        settingsStore.updateData { currentPreferences ->
            currentPreferences.toBuilder()
                .setLastSeenVersion(value)
                .build()
        }
    }
}
