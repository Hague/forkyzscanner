
package app.crossword.yourealwaysbe.forkyzscanner

import android.app.Application
import com.google.android.material.color.DynamicColors
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.runBlocking

import javax.inject.Inject

import dagger.hilt.android.HiltAndroidApp

import app.crossword.yourealwaysbe.forkyzscanner.settings.SettingsRepository

@HiltAndroidApp
class ForkyzScannerApplication : Application() {

    @Inject
    lateinit var settings : SettingsRepository

    override fun onCreate() {
        super.onCreate()

        // run blocking cos need to know before onCreate completes
        val settingsValue = runBlocking { settings.settingsFlow.first() }
        if (settingsValue.dynamicColors) {
            DynamicColors.applyToActivitiesIfAvailable(this)
        }
    }
}
