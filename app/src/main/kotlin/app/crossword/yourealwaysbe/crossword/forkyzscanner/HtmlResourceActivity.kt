
package app.crossword.yourealwaysbe.forkyzscanner

import android.os.Bundle
import androidx.core.text.HtmlCompat

import app.crossword.yourealwaysbe.forkyzscanner.databinding.HtmlResourceActivityBinding

abstract class HtmlResourceActivity(private val htmlStringId : Int)
        : ForkyzScannerBaseActivity () {

    private lateinit var binding : HtmlResourceActivityBinding

    override fun onCreate(savedInstanceState : Bundle?) {
        super.onCreate(savedInstanceState)

        binding = HtmlResourceActivityBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setSupportActionBar(binding.toolbar)
        setupBottomInsets(binding.main)
        setStatusBarElevation(binding.appBarLayout)

        binding.content.text = HtmlCompat.fromHtml(getString(htmlStringId), 0)
    }
}
