
package app.crossword.yourealwaysbe.forkyzscanner.scanning

import android.content.Context
import android.net.Uri
import androidx.arch.core.executor.testing.CountingTaskExecutorRule
import androidx.test.platform.app.InstrumentationRegistry

import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.util.concurrent.TimeUnit

import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Assert.fail
import org.junit.Before
import org.junit.Rule
import org.junit.Test

import app.crossword.yourealwaysbe.forkyzscanner.model.Puzzle.Clue

class CluesScannerTest {
    private val TEST_LANG = "test_eng"

    // needed to make sure all postvalues are done before testing
    @get:Rule
    public val countingTaskExecutorRule = CountingTaskExecutorRule()

    @Before
    fun setup() {
        val context = InstrumentationRegistry.getInstrumentation().targetContext
        try {
            CluesScannerTest::class.java
                .getResourceAsStream("/eng.traineddata")!!
                .use { input ->
                    CluesScanner.importLanguageModel(
                        context,
                        TEST_LANG + ".traineddata",
                        input,
                    )
                }
        } catch (e : IOException) {
            e.printStackTrace()
            fail()
        }
    }

    @After
    fun tearDown() {
        val context = InstrumentationRegistry.getInstrumentation().targetContext
        CluesScanner.deleteLanguageModel(context, TEST_LANG)
    }

    @Test
    fun testClues1() {
        val clues = getClues("/clues1.png")

        countingTaskExecutorRule.drainTasks(3, TimeUnit.SECONDS)

        assertEquals(clues.size, 4)
        assertEquals(clues[0].number.getValue(), "1")
        assertEquals(clues[0].hint.getValue(), "Here is no why")
        assertEquals(clues[1].number.getValue(), "2")
        assertEquals(clues[1].hint.getValue(), "There is many huh (4)")
        assertEquals(clues[2].number.getValue(), "3")
        assertEquals(
            clues[2].hint.getValue(),
            "Or is what what? And what's more (8, 4)"
        )
        assertEquals(clues[3].number.getValue(), "4")
        assertEquals(clues[3].hint.getValue(), "Two is four why (3)")
    }

    @Test
    fun testCluesCorruptClueStart() {
        val clues = getClues("/clues2.png")

        countingTaskExecutorRule.drainTasks(3, TimeUnit.SECONDS)

        assertEquals(clues.size, 2)
        assertEquals(clues[0].number.getValue(), "1")
        assertEquals(clues[0].hint.getValue(), "Here is no why (3)")
        assertEquals(clues[1].number.getValue(), "")
        assertEquals(clues[1].hint.getValue(), "N) There is many huh (4)")
    }

    private fun getClues(resourceName : String) : List<Clue> {
        val context = InstrumentationRegistry.getInstrumentation().targetContext
        val fullPath = extractFile(context, resourceName)
        return CluesScanner().scanCluesFromUri(context, TEST_LANG, fullPath)
    }

    // Code below borrowed and adapted from Tesseract4Android sample app

    private fun getTestDataPath(context : Context) : String {
        return context.getFilesDir().getAbsolutePath()
    }

    /**
     * Copies file into data path and returns new path
     */
    private fun extractFile(context : Context, resourceName: String) : Uri {
        val testDir = File(getTestDataPath(context), "testdata")
        if (!testDir.exists()) {
            testDir.mkdir()
        }
        val file = File(testDir, resourceName)
        // always overwrite in case of updates
        copyResourceFile(resourceName, file)
        return Uri.fromFile(file)
    }

    private fun copyResourceFile(
        resourceName : String,
        outFile : File,
    ) {
        try {
            CluesScannerTest::class.java
                .getResourceAsStream(resourceName)!!
                .use { input ->
                    FileOutputStream(outFile).use { output ->
                        input.copyTo(output)
                    }
                }
        } catch (e : IOException) {
            e.printStackTrace()
        }
    }
}
