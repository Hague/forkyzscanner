
package app.crossword.yourealwaysbe.forkyzscanner

import android.app.Dialog
import android.app.DownloadManager
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.net.Uri
import android.os.Bundle
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts.GetContent
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.dialog.MaterialAlertDialogBuilder

import dagger.hilt.android.AndroidEntryPoint

import app.crossword.yourealwaysbe.forkyzscanner.androidversion.versionUtils
import app.crossword.yourealwaysbe.forkyzscanner.databinding.LanguageModelItemBinding
import app.crossword.yourealwaysbe.forkyzscanner.databinding.SettingsActivityBinding
import app.crossword.yourealwaysbe.forkyzscanner.scanning.LanguageModels

@AndroidEntryPoint
class SettingsActivity : ForkyzScannerBaseActivity() {
    private val viewModel by viewModels<SettingsViewModel>()
    private lateinit var binding : SettingsActivityBinding
    private lateinit var languagesAdapter : LanguagesAdapter

    private val importLanguageModelLauncher =
        registerForActivityResult(GetContent()) { uri : Uri? ->
            uri?.let { viewModel.importLanguageModel(it) }
        }

    private val downloadCompleteReciever = object : BroadcastReceiver() {
        override fun onReceive(context : Context, intent : Intent) {
            when (intent.action) {
                LanguageModelDownloadReceiver.LANGUAGE_MODEL_DOWNLOADED -> {
                    viewModel.refreshLanguageModels()
                }
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = SettingsActivityBinding.inflate(layoutInflater)
        binding.lifecycleOwner = this
        binding.viewModel = viewModel

        setContentView(binding.root)
        setSupportActionBar(binding.toolbar)
        setupBottomInsets(binding.main)
        setStatusBarElevation(binding.appBarLayout)

        setupView()
        setupObservers()

        val filter = IntentFilter(
            LanguageModelDownloadReceiver.LANGUAGE_MODEL_DOWNLOADED,
        )
        ContextCompat.registerReceiver(
            this,
            downloadCompleteReciever,
            filter,
            ContextCompat.RECEIVER_EXPORTED
        )
    }

    override fun onDestroy() {
        unregisterReceiver(downloadCompleteReciever)
        super.onDestroy()
    }

    private fun setupView() {
        with(binding) {
            useDynamicSwitch.setOnCheckedChangeListener { _, checked ->
                viewModel?.setDynamicColors(checked)
            }
            importLanguageModelButton.setOnClickListener {
                importLanguageModelLauncher.launch("*/*")
            }
            downloadLanguageModelButton.setOnClickListener {
                downloadLanguageModel()
            }
            deleteLanguageModelButton.setOnClickListener {
                viewModel?.deleteSelectedLanguageModel()
            }
            releaseNotes.setOnClickListener {
                startActivity(
                    Intent(
                        this@SettingsActivity,
                        ReleaseNotesActivity::class.java
                    )
                )
            }
        }

        languagesAdapter = LanguagesAdapter()
        with(binding.languagesList) {
            layoutManager = LinearLayoutManager(context)
            itemAnimator = DefaultItemAnimator()
            addItemDecoration(
                DividerItemDecoration(context, DividerItemDecoration.VERTICAL)
            )
            adapter = languagesAdapter
        }
    }

    private fun setupObservers() {
        viewModel.languageModels.observe(
            this,
            { models -> languagesAdapter.submitList(models) },
        )
    }

    private fun downloadLanguageModel() {
        if (!versionUtils.hasNetworkConnection(this)) {
            Toast.makeText(
                this,
                R.string.download_but_no_active_network,
                Toast.LENGTH_LONG
            ).show()
        } else {
            DownloadLanguageModelDialog()
                .show(getSupportFragmentManager(), "DownloadLanguageModel")
        }
    }

    private val STRING_DIFF = object : DiffUtil.ItemCallback<String>() {
        override fun areItemsTheSame(
            oldName : String,
            newName : String,
        ) : Boolean = oldName == newName

        override fun areContentsTheSame(
            oldName : String,
            newName : String,
        ) : Boolean = oldName == newName
    }

    private inner class LanguagesAdapter
            : ListAdapter<String, LanguageModelHolder>(STRING_DIFF) {
        override fun onCreateViewHolder(
            parent : ViewGroup,
            viewType : Int,
        ) : LanguageModelHolder {
            val binding =
                LanguageModelItemBinding.inflate(
                    getLayoutInflater(),
                    parent,
                    false
                )
            return LanguageModelHolder(binding)
        }

        override fun onBindViewHolder(
            holder : LanguageModelHolder,
            position : Int,
        ) {
            holder.filename = getItem(position)
        }
    }

    private inner class LanguageModelHolder(
        val binding : LanguageModelItemBinding
    ) : RecyclerView.ViewHolder(binding.root) {
        var filename : String = ""
            set(value) {
                field = value
                binding.languageModelName.text = value
                binding.root.setOnClickListener {
                    viewModel.setSelectedLanguageModel(filename)
                }
                setChecked()
            }

        init {
            viewModel.liveSettings.observe(
                this@SettingsActivity,
                { setChecked() },
            )
        }

        private fun setChecked() {
            val selected = viewModel.liveSettings.value?.selectedLanguageModel
            binding.languageModelName.setChecked(filename == selected)
        }
    }

    class DownloadLanguageModelDialog() : DialogFragment() {
        override fun onCreateDialog(savedInstanceState : Bundle?) : Dialog {
            val context : Context = requireContext()

            val builder = MaterialAlertDialogBuilder(context)

            val names = LanguageModels.datafileNames

            builder.setTitle(R.string.download_language_model_dialog_title)
                .setSingleChoiceItems(
                    names,
                    -1,
                    { dialog, which ->
                        val downloadManager = ContextCompat.getSystemService(
                            context,
                            DownloadManager::class.java
                        )
                        val url = LanguageModels.getDatafileUrl(
                            names[which]
                        )
                        val request = DownloadManager.Request(Uri.parse(url))
                            .setNotificationVisibility(
                                DownloadManager.Request
                                    .VISIBILITY_VISIBLE_NOTIFY_COMPLETED
                            )
                        downloadManager?.enqueue(request)
                        dialog.dismiss()
                    }
                )

            return builder.create()
        }
    }
}
