
package app.crossword.yourealwaysbe.forkyzscanner

import android.app.Application
import android.net.Uri
import android.widget.Toast
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.launch

import java.io.IOException
import java.util.EnumMap
import javax.inject.Inject

import dagger.hilt.android.lifecycle.HiltViewModel

import app.crossword.yourealwaysbe.forkyzscanner.androidversion.versionUtils
import app.crossword.yourealwaysbe.forkyzscanner.model.Puzzle
import app.crossword.yourealwaysbe.forkyzscanner.model.Puzzle.Clue
import app.crossword.yourealwaysbe.forkyzscanner.model.Puzzle.ClueAdded
import app.crossword.yourealwaysbe.forkyzscanner.model.Puzzle.ClueChanged
import app.crossword.yourealwaysbe.forkyzscanner.model.Puzzle.ClueDeleted
import app.crossword.yourealwaysbe.forkyzscanner.model.Puzzle.ClueList
import app.crossword.yourealwaysbe.forkyzscanner.model.Puzzle.ClueListEvent
import app.crossword.yourealwaysbe.forkyzscanner.model.Puzzle.ClueListListener
import app.crossword.yourealwaysbe.forkyzscanner.model.Puzzle.ClueMoved
import app.crossword.yourealwaysbe.forkyzscanner.scanning.CluesScanner
import app.crossword.yourealwaysbe.forkyzscanner.scanning.GridScanner
import app.crossword.yourealwaysbe.forkyzscanner.settings.PuzzleManager
import app.crossword.yourealwaysbe.forkyzscanner.settings.SettingsRepository

@HiltViewModel
public class ForkyzScannerViewModel @Inject constructor(
    application : Application,
    private val settings : SettingsRepository,
) : AndroidViewModel(application) {

    private val _puzzle = MutableLiveData<Puzzle>()
    val puzzle : LiveData<Puzzle>
        get() = _puzzle
    private lateinit var clueListListener : ClueListListener
    init { loadPuzzle() }

    val nextCellNumber = MutableLiveData<Int?>(1)

    /**
     * Selected clue index in "single selection" model
     */
    val selectedClueIndex = EnumMap<ClueList, MutableLiveData<Int>>(
        ClueList::class.java,
    )

    /**
     * Selected clue indices in multi-select mode.
     *
     * If non-empty, then in multi mode
     */
    private val _multiSelectedClueIndices =
        EnumMap<ClueList, MutableLiveData<MutableSet<Int>>>(
            ClueList::class.java
        )
    val multiSelectedClueIndices : EnumMap<ClueList, out LiveData<out Set<Int>>>
        get() = _multiSelectedClueIndices

    init {
        selectedClueIndex[ClueList.ACROSS] = MutableLiveData<Int>(-1)
        selectedClueIndex[ClueList.DOWN] = MutableLiveData<Int>(-1)
        _multiSelectedClueIndices[ClueList.ACROSS] =
            MutableLiveData<MutableSet<Int>>(mutableSetOf<Int>())
        _multiSelectedClueIndices[ClueList.DOWN] =
            MutableLiveData<MutableSet<Int>>(mutableSetOf<Int>())
    }

    fun scanGridFromUri(imageUri : Uri) {
        viewModelScope.launch(Dispatchers.Default) {
            try {
                val scanner = GridScanner()
                val bitmap = versionUtils.getBitmapFromUri(
                    getApplication(),
                    imageUri,
                )

                val grid = scanner.scanGridFromBitmap(bitmap)

                viewModelScope.launch(Dispatchers.Main) {
                    puzzle.value?.grid?.value = grid
                    grid?.overwriteNumbersStandard()
                }
            } catch (e : IOException) {
                // TODO: something better than ignore
            }
        }
    }

    /**
     * Show a warning Toast if language model not set up
     *
     * If passes, run continuation
     */
    fun warnLanguageModelSetup(cont : () -> Unit) {
        viewModelScope.launch(Dispatchers.Default) {
            val app : Application = getApplication()
            val selectedModel =
                settings.settingsFlow.first().selectedLanguageModel

            val notReady = selectedModel.isEmpty() ||
                !CluesScanner.hasLanguageModel(app, selectedModel)

            if (notReady) {
                viewModelScope.launch(Dispatchers.Main) {
                    Toast.makeText(
                        app,
                        R.string.tesseract_not_ready,
                        Toast.LENGTH_LONG
                    ).show()
                }
            } else {
                viewModelScope.launch(Dispatchers.Main) { cont() }
            }
        }
    }

    /**
     * Scan clues from an image file
     *
     * Adds to puzzle clue list.
     */
    fun scanCluesFromUri(list : ClueList, imageUri : Uri) {
        viewModelScope.launch(Dispatchers.Default) {
            try {
                val app : Application = getApplication()

                val selectedModel =
                    settings.settingsFlow.first().selectedLanguageModel

                val notReady = selectedModel.isEmpty() ||
                    !CluesScanner.hasLanguageModel(app, selectedModel)

                if (notReady) {
                    viewModelScope.launch(Dispatchers.Main) {
                        Toast.makeText(
                            app,
                            R.string.tesseract_not_ready,
                            Toast.LENGTH_LONG
                        ).show()
                    }
                } else {
                    val scanner = CluesScanner()
                    val clues = scanner.scanCluesFromUri(
                        app,
                        selectedModel,
                        imageUri
                    )
                    puzzle.value?.addClues(list, clues)
                }
            } catch (e : IOException) {
                // TODO: something better than ignore
            }
        }
    }

    /**
     * Scan metadata notes from image
     */
    fun scanNotesFromUri(imageUri : Uri) {
        viewModelScope.launch(Dispatchers.Default) {
            try {
                val app : Application = getApplication()

                val selectedModel =
                    settings.settingsFlow.first().selectedLanguageModel

                val notReady = selectedModel.isEmpty() ||
                    !CluesScanner.hasLanguageModel(app, selectedModel)

                if (notReady) {
                    viewModelScope.launch(Dispatchers.Main) {
                        Toast.makeText(
                            app,
                            R.string.tesseract_not_ready,
                            Toast.LENGTH_LONG
                        ).show()
                    }
                } else {
                    val scanner = CluesScanner()
                    val notes = scanner.scanNotesFromUri(
                        app,
                        selectedModel,
                        imageUri
                    )
                    puzzle.value?.let { it.notes.postValue(notes) }
                }
            } catch (e : IOException) {
                // TODO: something better than ignore
            }
        }
    }


    fun getSelectedClue(list : ClueList) : Clue? {
        val index = selectedClueIndex[list]?.value
        val clues = puzzle.value?.getClueList(list)
        if (
            clues == null || index == null || index < 0 || index >= clues.size
        ) {
            return null
        } else {
            return clues[index]
        }
    }

    fun toggleMultiSelected(list : ClueList, index : Int) {
        val clues = puzzle.value?.getClueList(list)
        if (clues == null || index < 0 || index > clues.size) return

        val liveMultis = _multiSelectedClueIndices[list]
        liveMultis?.value?.let { multies ->
            if (index in multies) multies.remove(index)
            else multies.add(index)
            liveMultis.postValue(multies)
        }
    }

    fun isMultiSelect(list : ClueList) : Boolean {
        return !(multiSelectedClueIndices[list]?.value?.isEmpty() ?: true)
    }

    fun clearMultiSelection(list : ClueList) {
        val liveSelectedSet = _multiSelectedClueIndices[list]
        liveSelectedSet?.value?.let { selectedSet ->
            selectedSet.clear()
            liveSelectedSet.postValue(selectedSet)
        }
    }

    fun deleteMultiSelectedClues(list : ClueList) {
        multiSelectedClueIndices[list]?.value?.let { selectedSet ->
            puzzle.value?.deleteClues(list, selectedSet)
        }
    }

    fun moveMultiSelectedCluesUp(list : ClueList) {
        multiSelectedClueIndices[list]?.value?.let { selectedSet ->
            puzzle.value?.moveCluesUp(list, selectedSet)
        }
    }

    fun moveMultiSelectedCluesDown(list : ClueList) {
        multiSelectedClueIndices[list]?.value?.let { selectedSet ->
            puzzle.value?.moveCluesDown(list, selectedSet)
        }
    }

    fun savePuzzle() {
        puzzle.value?.let { puzzle ->
            viewModelScope.launch {
                PuzzleManager.savePuzzle(getApplication(), puzzle)
            }
        }
    }

    fun loadPuzzle() {
        viewModelScope.launch {
            val puzzle = PuzzleManager.loadPuzzle(getApplication())
            if (puzzle != null) setPuzzle(puzzle)
            else newPuzzle()
        }
    }

    fun newPuzzle() {
        setPuzzle(Puzzle(15, 15))
    }

    private fun setPuzzle(puzzle : Puzzle) {
        this._puzzle.postValue(puzzle)

        // listener updates selected indices as clues change
        clueListListener = object : ClueListListener {
            override fun onClueListChange(
                list : ClueList,
                newList : List<Clue>,
                changed : List<ClueListEvent>
            ) {
                var selIndex = selectedClueIndex[list]?.value
                var multiSels = multiSelectedClueIndices[list]?.value?.let {
                    ArrayList(it)
                }

                changed.forEach { change ->
                    selIndex?.let { selIndex = updateIndex(it, change) }
                    multiSels?.forEachIndexed { listIndex, index ->
                        multiSels[listIndex] = updateIndex(index, change)
                    }
                }

                selIndex?.let { selectedClueIndex[list]?.postValue(selIndex) }
                multiSels?.let {
                    _multiSelectedClueIndices[list]?.postValue(
                        multiSels.filter({ it >= 0 }).toMutableSet()
                    )
                }
            }

            /**
             * Returns new value of index after applying change
             */
            private fun updateIndex(index : Int, change : ClueListEvent) : Int {
                if (index < 0) return index

                when (change) {
                    is ClueChanged -> { return index }
                    is ClueAdded -> {
                        if (change.index <= index) return index + 1
                        else return index
                    }
                    is ClueMoved -> {
                        if (index == change.src) return change.dst

                        var newIndex = index

                        if (newIndex > change.src) newIndex -= 1
                        if (newIndex >= change.dst) newIndex += 1

                        return newIndex
                    }
                    is ClueDeleted -> {
                        if (index == change.index) return -1
                        else if (index > change.index) return index - 1
                        else return index
                    }
                }
            }
        }

        puzzle.addClueListListener(ClueList.ACROSS, clueListListener)
        puzzle.addClueListListener(ClueList.DOWN, clueListListener)
    }
}
