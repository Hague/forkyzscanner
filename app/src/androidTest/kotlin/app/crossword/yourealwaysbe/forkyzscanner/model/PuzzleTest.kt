
package app.crossword.yourealwaysbe.forkyzscanner.model

import androidx.arch.core.executor.testing.CountingTaskExecutorRule

import java.util.concurrent.TimeUnit

import org.junit.Assert.assertEquals
import org.junit.Assert.assertFalse
import org.junit.Assert.assertNull
import org.junit.Assert.assertTrue
import org.junit.Rule
import org.junit.Test

import app.crossword.yourealwaysbe.forkyzscanner.model.Puzzle.Clue
import app.crossword.yourealwaysbe.forkyzscanner.model.Puzzle.ClueAdded
import app.crossword.yourealwaysbe.forkyzscanner.model.Puzzle.ClueChanged
import app.crossword.yourealwaysbe.forkyzscanner.model.Puzzle.ClueDeleted
import app.crossword.yourealwaysbe.forkyzscanner.model.Puzzle.ClueList
import app.crossword.yourealwaysbe.forkyzscanner.model.Puzzle.ClueListEvent
import app.crossword.yourealwaysbe.forkyzscanner.model.Puzzle.ClueListListener
import app.crossword.yourealwaysbe.forkyzscanner.model.Puzzle.ClueMoved
import app.crossword.yourealwaysbe.forkyzscanner.model.Puzzle.Grid

class PuzzleTest {
    // needed to make sure all postvalues are done before testing
    @get:Rule
    public val countingTaskExecutorRule = CountingTaskExecutorRule()

    @Test
    fun testShrinkGrid() {
        val grid = getGrid(
            arrayOf(
                arrayOf(1, 1, 1, 1, 1, 1),
                arrayOf(1, 0, 1, 0, 0, 0),
                arrayOf(1, 0, 1, 1, 1, 1),
                arrayOf(1, 0, 1, 0, 1, 0),
                arrayOf(1, 1, 1, 1, 1, 1),
            )
        )

        val gridT = Grid(grid, 1, 0, 0, 0)
        val checkT = arrayOf(
            arrayOf(1, 0, 1, 0, 0, 0),
            arrayOf(1, 0, 1, 1, 1, 1),
            arrayOf(1, 0, 1, 0, 1, 0),
            arrayOf(1, 1, 1, 1, 1, 1),
        )

        val gridL = Grid(grid, 0, 1, 0, 0)
        val checkL = arrayOf(
            arrayOf(1, 1, 1, 1, 1),
            arrayOf(0, 1, 0, 0, 0),
            arrayOf(0, 1, 1, 1, 1),
            arrayOf(0, 1, 0, 1, 0),
            arrayOf(1, 1, 1, 1, 1),
        )

        val gridR = Grid(grid, 0, 0, -1, 0)
        val checkR = arrayOf(
            arrayOf(1, 1, 1, 1, 1),
            arrayOf(1, 0, 1, 0, 0),
            arrayOf(1, 0, 1, 1, 1),
            arrayOf(1, 0, 1, 0, 1),
            arrayOf(1, 1, 1, 1, 1),
        )

        val gridB = Grid(grid, 0, 0, 0, -1)
        val checkB = arrayOf(
            arrayOf(1, 1, 1, 1, 1, 1),
            arrayOf(1, 0, 1, 0, 0, 0),
            arrayOf(1, 0, 1, 1, 1, 1),
            arrayOf(1, 0, 1, 0, 1, 0),
        )

        waitPosts()

        checkBlocks(gridT, checkT)
        checkBlocks(gridL, checkL)
        checkBlocks(gridR, checkR)
        checkBlocks(gridB, checkB)
    }

    @Test
    fun testGrowGrid() {
        val grid = getGrid(
            arrayOf(
                arrayOf(1, 1, 1, 1, 1, 1),
                arrayOf(1, 0, 1, 0, 0, 0),
                arrayOf(1, 0, 1, 1, 1, 1),
                arrayOf(1, 0, 1, 0, 1, 0),
                arrayOf(1, 1, 1, 1, 1, 1),
            )
        )

        val gridT = Grid(grid, -1, 0, 0, 0)
        val checkT = arrayOf(
            arrayOf(1, 1, 1, 1, 1, 1),
            arrayOf(1, 1, 1, 1, 1, 1),
            arrayOf(1, 0, 1, 0, 0, 0),
            arrayOf(1, 0, 1, 1, 1, 1),
            arrayOf(1, 0, 1, 0, 1, 0),
            arrayOf(1, 1, 1, 1, 1, 1),
        )

        val gridL = Grid(grid, 0, -1, 0, 0)
        val checkL = arrayOf(
            arrayOf(1, 1, 1, 1, 1, 1, 1),
            arrayOf(1, 1, 0, 1, 0, 0, 0),
            arrayOf(1, 1, 0, 1, 1, 1, 1),
            arrayOf(1, 1, 0, 1, 0, 1, 0),
            arrayOf(1, 1, 1, 1, 1, 1, 1),
        )

        val gridR = Grid(grid, 0, 0, 1, 0)
        val checkR = arrayOf(
            arrayOf(1, 1, 1, 1, 1, 1, 1),
            arrayOf(1, 0, 1, 0, 0, 0, 1),
            arrayOf(1, 0, 1, 1, 1, 1, 1),
            arrayOf(1, 0, 1, 0, 1, 0, 1),
            arrayOf(1, 1, 1, 1, 1, 1, 1),
        )

        val gridB = Grid(grid, 0, 0, 0, 1)
        val checkB = arrayOf(
            arrayOf(1, 1, 1, 1, 1, 1),
            arrayOf(1, 0, 1, 0, 0, 0),
            arrayOf(1, 0, 1, 1, 1, 1),
            arrayOf(1, 0, 1, 0, 1, 0),
            arrayOf(1, 1, 1, 1, 1, 1),
            arrayOf(1, 1, 1, 1, 1, 1),
        )

        waitPosts()

        checkBlocks(gridT, checkT)
        checkBlocks(gridL, checkL)
        checkBlocks(gridR, checkR)
        checkBlocks(gridB, checkB)
    }

    @Test
    fun testStandardNumbering() {
        val grid = getGrid(
            arrayOf(
                arrayOf(1, 1, 1, 1, 1, 1),
                arrayOf(1, 0, 1, 0, 0, 0),
                arrayOf(1, 0, 1, 1, 1, 1),
                arrayOf(1, 0, 1, 0, 1, 0),
                arrayOf(1, 1, 1, 1, 1, 1),
            )
        )

        grid.overwriteNumbersStandard()
        waitPosts()

        val numbers = arrayOf(
            arrayOf(1, 0, 2, 0, 0, 0),
            arrayOf(0, 0, 0, 0, 0, 0),
            arrayOf(0, 0, 3, 0, 4, 0),
            arrayOf(0, 0, 0, 0, 0, 0),
            arrayOf(5, 0, 0, 0, 0, 0),
        )

        checkNumbers(grid, numbers)
    }

    @Test
    fun testAddClue() {
        val puzzle = Puzzle(1, 1)
        waitPosts()

        val listenerAcross = makeListenerExpecting(
            listOf(listOf(ClueAdded(0)), listOf(ClueAdded(1)))
        )
        val listenerDown = makeListenerExpecting(
            listOf(listOf(ClueAdded(0)))
        )

        assertEquals(puzzle.getClueList(ClueList.ACROSS).size, 0)
        assertEquals(puzzle.getClueList(ClueList.DOWN).size, 0)

        puzzle.addClueListListener(ClueList.ACROSS, listenerAcross)
        puzzle.addClueListListener(ClueList.DOWN, listenerDown)

        puzzle.addClue(ClueList.ACROSS, Clue("1", "."))
        waitPosts()

        assertEquals(puzzle.getClueList(ClueList.ACROSS).size, 1)
        assertEquals(puzzle.getClueList(ClueList.DOWN).size, 0)

        puzzle.addClue(ClueList.ACROSS, Clue("2", "."))
        waitPosts()

        assertEquals(puzzle.getClueList(ClueList.ACROSS).size, 2)
        assertEquals(puzzle.getClueList(ClueList.DOWN).size, 0)

        puzzle.addClue(ClueList.DOWN, Clue("3", "."))
        waitPosts()

        val across = puzzle.getClueList(ClueList.ACROSS)
        val down = puzzle.getClueList(ClueList.DOWN)

        assertTrue(listenerAcross.passed())
        assertTrue(listenerDown.passed())
        assertEquals(across.size, 2)
        assertEquals(down.size, 1)
        assertEquals(across[0].number.value, "1")
        assertEquals(across[1].number.value, "2")
        assertEquals(down[0].number.value, "3")
    }

    @Test
    fun testAddClues() {
        val puzzle = Puzzle(1, 1)
        waitPosts()

        val listenerAcross = makeListenerExpecting(
            listOf(
                listOf(ClueAdded(0), ClueAdded(1)),
                listOf(ClueAdded(2)),
            )
        )
        val listenerDown = makeListenerExpecting(
            listOf(listOf(ClueAdded(0))),
        )

        puzzle.addClueListListener(ClueList.ACROSS, listenerAcross)
        puzzle.addClueListListener(ClueList.DOWN, listenerDown)

        assertEquals(puzzle.getClueList(ClueList.ACROSS).size, 0)
        assertEquals(puzzle.getClueList(ClueList.DOWN).size, 0)

        puzzle.addClues(ClueList.ACROSS, listOf(Clue("1", "."), Clue("2", ".")))
        waitPosts()

        assertEquals(puzzle.getClueList(ClueList.ACROSS).size, 2)
        assertEquals(puzzle.getClueList(ClueList.DOWN).size, 0)

        puzzle.addClues(ClueList.ACROSS, listOf())
        waitPosts()

        assertEquals(puzzle.getClueList(ClueList.ACROSS).size, 2)
        assertEquals(puzzle.getClueList(ClueList.DOWN).size, 0)

        puzzle.addClues(ClueList.DOWN, listOf(Clue("3", ".")))
        puzzle.addClues(ClueList.ACROSS, listOf(Clue("4", ".")))
        waitPosts()

        val across = puzzle.getClueList(ClueList.ACROSS)
        val down = puzzle.getClueList(ClueList.DOWN)

        assertTrue(listenerAcross.passed())
        assertTrue(listenerDown.passed())
        assertEquals(across.size, 3)
        assertEquals(down.size, 1)
        assertEquals(across[0].number.value, "1")
        assertEquals(across[1].number.value, "2")
        assertEquals(down[0].number.value, "3")
        assertEquals(across[2].number.value, "4")
    }

    @Test
    fun testSplitClue() {
        val puzzle = Puzzle(1, 1)
        waitPosts()

        val listener = makeListenerExpecting(
            listOf(
                listOf(ClueAdded(0), ClueAdded(1)),
                listOf(ClueChanged(0), ClueAdded(1)),
            ),
        )
        puzzle.addClueListListener(ClueList.ACROSS, listener)

        puzzle.addClues(
            ClueList.ACROSS,
            listOf(Clue("1", "testtest2"), Clue("2", "test3"))
        )
        waitPosts()

        puzzle.splitClue(ClueList.ACROSS, 0, 4)
        waitPosts()

        val list = puzzle.getClueList(ClueList.ACROSS)

        assertTrue(listener.passed())
        assertEquals(list.size, 3)
        assertEquals(list[0].number.value, "1")
        assertEquals(list[0].hint.value, "test")
        assertEquals(list[1].number.value, "")
        assertEquals(list[1].hint.value, "test2")
        assertEquals(list[2].number.value, "2")
        assertEquals(list[2].hint.value, "test3")
    }

    @Test
    fun testSplitOnlyClue() {
        val puzzle = Puzzle(1, 1)
        waitPosts()

        val listener = makeListenerExpecting(
            listOf(
                listOf(ClueAdded(0)),
                listOf(ClueChanged(0), ClueAdded(1)),
            ),
        )
        puzzle.addClueListListener(ClueList.ACROSS, listener)

        puzzle.addClues(ClueList.ACROSS, listOf(Clue("1", "testtest2")))
        waitPosts()

        puzzle.splitClue(ClueList.ACROSS, 0, 4)
        waitPosts()

        val list = puzzle.getClueList(ClueList.ACROSS)

        assertTrue(listener.passed())
        assertEquals(list.size, 2)
        assertEquals(list[0].number.value, "1")
        assertEquals(list[0].hint.value, "test")
        assertEquals(list[1].number.value, "")
        assertEquals(list[1].hint.value, "test2")
    }

    @Test
    fun testSplitEndOfClue() {
        val puzzle = Puzzle(1, 1)
        waitPosts()

        val listener = makeListenerExpecting(
            listOf(
                listOf(ClueAdded(0)),
                listOf(ClueChanged(0), ClueAdded(1)),
            ),
        )
        puzzle.addClueListListener(ClueList.ACROSS, listener)

        val hint = "testtest"

        puzzle.addClues(ClueList.ACROSS, listOf(Clue("1", hint)))
        waitPosts()

        puzzle.splitClue(ClueList.ACROSS, 0, hint.length)
        waitPosts()

        val list = puzzle.getClueList(ClueList.ACROSS)

        assertTrue(listener.passed())
        assertEquals(list.size, 2)
        assertEquals(list[0].number.value, "1")
        assertEquals(list[0].hint.value, hint)
        assertEquals(list[1].number.value, "")
        assertEquals(list[1].hint.value, "")
    }

    @Test
    fun testDeleteClues() {
        val puzzle = Puzzle(1, 1)
        waitPosts()

        val listener = makeListenerExpecting(
            listOf(
                (0..3).map { ClueAdded(it) },
                listOf(ClueDeleted(1), ClueDeleted(2)),
            ),
        )
        puzzle.addClueListListener(ClueList.ACROSS, listener)

        puzzle.addClues(
            ClueList.ACROSS,
            listOf(
                Clue("1", "clue1"),
                Clue("2", "clue2"),
                Clue("3", "clue3"),
                Clue("4", "clue4"),
            ),
        )
        waitPosts()

        puzzle.deleteClues(ClueList.ACROSS, setOf(1, 3, 5))
        waitPosts()

        val list = puzzle.getClueList(ClueList.ACROSS)

        assertTrue(listener.passed())
        assertEquals(list.size, 2)
        assertEquals(list[0].number.value, "1")
        assertEquals(list[0].hint.value, "clue1")
        assertEquals(list[1].number.value, "3")
        assertEquals(list[1].hint.value, "clue3")
    }

    @Test
    fun testMoveCluesUp() {
        val puzzle = Puzzle(1, 1)
        waitPosts()

        val listener = makeListenerExpecting(
            listOf(
                (0..3).map { ClueAdded(it) },
                listOf(ClueMoved(1, 0), ClueMoved(3, 2)),
            ),
        )
        puzzle.addClueListListener(ClueList.ACROSS, listener)

        puzzle.addClues(
            ClueList.ACROSS,
            listOf(
                Clue("1", "clue1"),
                Clue("2", "clue2"),
                Clue("3", "clue3"),
                Clue("4", "clue4"),
            ),
        )
        waitPosts()

        puzzle.moveCluesUp(ClueList.ACROSS, setOf(1, 3))
        waitPosts()

        val list = puzzle.getClueList(ClueList.ACROSS)

        assertTrue(listener.passed())
        assertEquals(list.size, 4)
        assertEquals(list[0].number.value, "2")
        assertEquals(list[0].hint.value, "clue2")
        assertEquals(list[1].number.value, "1")
        assertEquals(list[1].hint.value, "clue1")
        assertEquals(list[2].number.value, "4")
        assertEquals(list[2].hint.value, "clue4")
        assertEquals(list[3].number.value, "3")
        assertEquals(list[3].hint.value, "clue3")
    }

    @Test
    fun testMoveCluesUpWithFirst() {
        val puzzle = Puzzle(1, 1)
        waitPosts()

        val listener = makeListenerExpecting(
            listOf(
                (0..3).map { ClueAdded(it) },
            ),
        )
        puzzle.addClueListListener(ClueList.ACROSS, listener)

        puzzle.addClues(
            ClueList.ACROSS,
            listOf(
                Clue("1", "clue1"),
                Clue("2", "clue2"),
                Clue("3", "clue3"),
                Clue("4", "clue4"),
            ),
        )
        waitPosts()

        puzzle.moveCluesUp(ClueList.ACROSS, setOf(0, 1, 3))
        waitPosts()

        val list = puzzle.getClueList(ClueList.ACROSS)

        assertTrue(listener.passed())
        assertEquals(list.size, 4)
        assertEquals(list[0].number.value, "1")
        assertEquals(list[0].hint.value, "clue1")
        assertEquals(list[1].number.value, "2")
        assertEquals(list[1].hint.value, "clue2")
        assertEquals(list[2].number.value, "3")
        assertEquals(list[2].hint.value, "clue3")
        assertEquals(list[3].number.value, "4")
        assertEquals(list[3].hint.value, "clue4")
    }

    @Test
    fun testMoveCluesDown() {
        val puzzle = Puzzle(1, 1)
        waitPosts()

        val listener = makeListenerExpecting(
            listOf(
                (0..3).map { ClueAdded(it) },
                listOf(ClueMoved(2, 3), ClueMoved(0, 1)),
            ),
        )
        puzzle.addClueListListener(ClueList.ACROSS, listener)

        puzzle.addClues(
            ClueList.ACROSS,
            listOf(
                Clue("1", "clue1"),
                Clue("2", "clue2"),
                Clue("3", "clue3"),
                Clue("4", "clue4"),
            ),
        )
        waitPosts()

        puzzle.moveCluesDown(ClueList.ACROSS, setOf(0, 2))
        waitPosts()

        val list = puzzle.getClueList(ClueList.ACROSS)

        assertTrue(listener.passed())
        assertEquals(list.size, 4)
        assertEquals(list[0].number.value, "2")
        assertEquals(list[0].hint.value, "clue2")
        assertEquals(list[1].number.value, "1")
        assertEquals(list[1].hint.value, "clue1")
        assertEquals(list[2].number.value, "4")
        assertEquals(list[2].hint.value, "clue4")
        assertEquals(list[3].number.value, "3")
        assertEquals(list[3].hint.value, "clue3")
    }

    @Test
    fun testMoveCluesDownWithLast() {
        val puzzle = Puzzle(1, 1)
        waitPosts()

        val listener = makeListenerExpecting(
            listOf(
                (0..3).map { ClueAdded(it) },
            ),
        )
        puzzle.addClueListListener(ClueList.ACROSS, listener)

        puzzle.addClues(
            ClueList.ACROSS,
            listOf(
                Clue("1", "clue1"),
                Clue("2", "clue2"),
                Clue("3", "clue3"),
                Clue("4", "clue4"),
            ),
        )
        waitPosts()

        puzzle.moveCluesDown(ClueList.ACROSS, setOf(0, 1, 3))
        waitPosts()

        assertTrue(listener.passed())

        val list = puzzle.getClueList(ClueList.ACROSS)

        assertTrue(listener.passed())
        assertEquals(list.size, 4)
        assertEquals(list[0].number.value, "1")
        assertEquals(list[0].hint.value, "clue1")
        assertEquals(list[1].number.value, "2")
        assertEquals(list[1].hint.value, "clue2")
        assertEquals(list[2].number.value, "3")
        assertEquals(list[2].hint.value, "clue3")
        assertEquals(list[3].number.value, "4")
        assertEquals(list[3].hint.value, "clue4")
    }

    @Test
    fun testHasNumber() {
        val grid = getGrid(
            arrayOf(
                arrayOf(1, 1, 1, 1, 1, 1),
                arrayOf(1, 0, 1, 0, 0, 0),
                arrayOf(1, 0, 1, 1, 1, 1),
                arrayOf(1, 0, 1, 0, 1, 0),
                arrayOf(1, 1, 1, 1, 1, 1),
            )
        )

        grid.overwriteNumbersStandard()

        val puzzle = Puzzle(grid)

        waitPosts()

        assertTrue(puzzle.hasNumber(1, ClueList.ACROSS))
        assertTrue(puzzle.hasNumber(1, ClueList.DOWN))
        assertFalse(puzzle.hasNumber(2, ClueList.ACROSS))
        assertTrue(puzzle.hasNumber(2, ClueList.DOWN))
        assertTrue(puzzle.hasNumber(3, ClueList.ACROSS))
        assertFalse(puzzle.hasNumber(3, ClueList.DOWN))
    }

    @Test
    fun testGetNumbers() {
        val grid = getGrid(
            arrayOf(
                arrayOf(1, 1, 1, 1, 1, 1),
                arrayOf(1, 0, 1, 0, 0, 0),
                arrayOf(1, 0, 1, 1, 1, 1),
                arrayOf(1, 0, 1, 0, 1, 0),
                arrayOf(1, 1, 1, 1, 1, 1),
            )
        )

        grid.overwriteNumbersStandard()

        val puzzle = Puzzle(grid)

        waitPosts()

        assertEquals(
            puzzle.getNumbers(ClueList.ACROSS),
            setOf(1, 3, 5)
        )
        assertEquals(
            puzzle.getNumbers(ClueList.DOWN),
            setOf(1, 2, 4)
        )
    }

    /**
     * Turns a 0/1 matrix into a grid of cells
     */
    private fun getGrid(cells : Array<Array<Int>>) : Grid {
        val height = cells.size
        val width = cells[0].size
        val grid = Grid(width, height)

        for (row in 0..(height - 1)) {
            for (col in 0..(width - 1)) {
                grid.boxes[row][col].isBlock.postValue(cells[row][col] == 0)
            }
        }

        waitPosts()

        return grid
    }

    /**
     * Checks grid has given numbers
     *
     * 0 mean no number, else >0
     */
    private fun checkNumbers(grid : Grid, numbers : Array<Array<Int>>) {
        grid.boxes.forEach { row ->
            row.forEach { box ->
                val expectedNum = numbers[box.row][box.col]
                val gridNum = box.number.getValue()
                if (expectedNum == 0) {
                    assertNull(gridNum)
                } else {
                    assertEquals(gridNum, expectedNum)
                }
            }
        }
    }

    /**
     * Checks grid has given blocks
     *
     * 0 is a block
     */
    private fun checkBlocks(grid : Grid, blocks : Array<Array<Int>>) {
        assertEquals(grid.height, blocks.size)
        if (blocks.size > 0)
            assertEquals(grid.width, blocks[0].size)

        grid.boxes.forEach { row ->
            row.forEach { box ->
                val expectedBlock = blocks[box.row][box.col] == 0
                val isBlock = box.isBlock.getValue()
                assertEquals(isBlock, expectedBlock)
            }
        }
    }

    /**
     * Wait for all postValues to complete
     */
    private fun waitPosts() {
        countingTaskExecutorRule.drainTasks(3, TimeUnit.SECONDS)
    }

    private fun makeListenerExpecting(
        expectedChanges : List<List<ClueListEvent>>
    ) : TestClueListListener {
        return object : TestClueListListener {
            private var seenTo = 0

            override fun onClueListChange(
                list : ClueList,
                newList : List<Clue>,
                changed : List<ClueListEvent>,
            ) {
                if (seenTo < 0 || seenTo >= expectedChanges.size) {
                    seenTo = -1
                } else if (changed == expectedChanges[seenTo]) {
                    seenTo += 1
                } else {
                    seenTo = -1
                }
            }

            override fun passed() = seenTo == expectedChanges.size
        }
    }

    private interface TestClueListListener : ClueListListener {
        fun passed() : Boolean
    }
}
