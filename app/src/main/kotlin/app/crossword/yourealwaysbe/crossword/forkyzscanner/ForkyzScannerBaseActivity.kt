
package app.crossword.yourealwaysbe.forkyzscanner

import android.os.Bundle
import android.view.View
import android.view.ViewGroup.MarginLayoutParams
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import androidx.core.view.WindowCompat
import androidx.core.view.WindowInsetsCompat
import com.google.android.material.appbar.AppBarLayout
import com.google.android.material.shape.MaterialShapeDrawable

import app.crossword.yourealwaysbe.forkyzscanner.databinding.HtmlResourceActivityBinding

abstract class ForkyzScannerBaseActivity() : AppCompatActivity() {
    override fun onCreate(savedInstanceState : Bundle?) {
        super.onCreate(savedInstanceState)
        WindowCompat.setDecorFitsSystemWindows(window, false);
    }

    /**
     * Call on the bottommost view so it does not overlap system windows
     */
    protected fun setupBottomInsets(view : View) { setupInsets(view, true) }

    /**
     * Call on the views so it does not overlap system windows on side
     */
    protected fun setupSideInsets(view : View) { setupInsets(view, false) }

    /**
     * Set the elevation to match the app bar
     */
    protected fun setStatusBarElevation(appBarLayout : AppBarLayout) {
        appBarLayout.setStatusBarForeground(
            MaterialShapeDrawable.createWithElevationOverlay(this)
        )
    }

    /**
     * Add insets against sides and possibly bottom for view
     */
    private fun setupInsets(view : View, includeBottom : Boolean) {
        val origMlp = view.getLayoutParams() as MarginLayoutParams
        // keep own copies because above object will change with view
        val origLeftMargin = origMlp.leftMargin
        val origBottomMargin = origMlp.bottomMargin
        val origRightMargin = origMlp.rightMargin

        ViewCompat.setOnApplyWindowInsetsListener(
            view,
            { v, windowInsets ->
                val insets = windowInsets.getInsets(
                    WindowInsetsCompat.Type.systemBars() or
                    WindowInsetsCompat.Type.ime()
                )

                val mlp = v.getLayoutParams() as MarginLayoutParams
                mlp.leftMargin = insets.left + origLeftMargin
                mlp.rightMargin = insets.right + origRightMargin
                if (includeBottom)
                    mlp.bottomMargin = insets.bottom + origBottomMargin

                v.setLayoutParams(mlp)

                WindowInsetsCompat.CONSUMED
            }
        )
    }

   /**
     * Call on the bottommost view so it does not overlap system windows
     *
     * Some views, such as material navigation bar, do it automatically, so no
     * need to call.
     */
    protected fun setupInsets(view : View) {
        val origMlp = view.getLayoutParams() as MarginLayoutParams
        // keep own copies because above object will change with view
        val origLeftMargin = origMlp.leftMargin
        val origBottomMargin = origMlp.bottomMargin
        val origRightMargin = origMlp.rightMargin

        ViewCompat.setOnApplyWindowInsetsListener(
            view,
            { v, windowInsets ->
                val insets = windowInsets.getInsets(
                    WindowInsetsCompat.Type.systemBars() or
                    WindowInsetsCompat.Type.ime()
                )

                val mlp = v.getLayoutParams() as MarginLayoutParams
                mlp.leftMargin = insets.left + origLeftMargin
                mlp.bottomMargin = insets.bottom + origBottomMargin
                mlp.rightMargin = insets.right + origRightMargin
                v.setLayoutParams(mlp)

                WindowInsetsCompat.CONSUMED
            }
        )
    }


}
